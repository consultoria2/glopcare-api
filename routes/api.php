<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CalendlyKeyController;
use App\Http\Controllers\DataDeletionController;
use App\Http\Controllers\GlopcardController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PolicyController;
use App\Http\Controllers\PrivacyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/register/phone', [AuthController::class, 'phoneRegister']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/login/phone', [AuthController::class, 'phoneLogin']);
Route::post('/login/google', [AuthController::class, 'googleLogin']);
Route::post('/login/facebook', [AuthController::class, 'facebookLogin']);
Route::get('/verify/{verification_token}', [AuthController::class, 'verify']);
Route::post('/profile/set', [UserProfileController::class, 'setProfile'])->middleware('auth:sanctum');
Route::get('/profile/get', [UserProfileController::class, 'getProfile'])->middleware('auth:sanctum');
Route::post('/check-device-id', [AuthController::class, 'checkDeviceId'])->middleware(['auth:sanctum', 'checkDeviceId']);

// Glopcard methods
Route::post('/glopcard/set', [GlopcardController::class, 'setGlopcard'])->middleware('auth:sanctum');
Route::get('/glopcard/get', [GlopcardController::class, 'getPrivateGlopcard'])->middleware('auth:sanctum');
Route::get('/glopcard/templates', [GlopcardController::class, 'listTemplates'])->middleware('auth:sanctum');
Route::get('/glopcard/{uid}', [GlopcardController::class, 'getPublicGlopcard']);

//-----------------------------------------------------------------------------------------
// CATALOG METHODS
//-----------------------------------------------------------------------------------------
Route::post('/catalog/set', [GlopcardController::class, 'setCatalog'])->middleware('auth:sanctum');
Route::post('/catalog/gallery/sort', [GlopcardController::class, 'sortCatalogGallery'])->middleware('auth:sanctum');
Route::get('/catalog/get', [GlopcardController::class, 'getPrivateCatalog'])->middleware('auth:sanctum');
Route::get('/catalog/{uid}', [GlopcardController::class, 'getPublicCatalog']);

Route::get('/catalog/categories/list/{uid}', [GlopcardController::class, 'getPublicCategories']);
Route::get('/catalog/groups/list/{uid}', [GlopcardController::class, 'getPublicGroups']);
Route::get('/catalog/list/{uid}', [GlopcardController::class, 'getPublicCatalogItems']);
Route::get('/catalog/product/{uid}/{product_id}', [GlopcardController::class, 'getPublicCatalogProduct']);
Route::get('/catalog/list/similars/{uid}/{product_id}', [GlopcardController::class, 'getPublicCatalogProductSimilars']);

//-----------------------------------------------------------------------------------------
// PAYMENT METHODS
//-----------------------------------------------------------------------------------------
Route::get('/payment_methods/list/{uid}', [GlopcardController::class, 'getPublicPaymentMethods']);

//-----------------------------------------------------------------------------------------
//  PRODUCTS AND SERVICES PRIVATE ACCESS
//-----------------------------------------------------------------------------------------

Route::get('/products/groups/list', [ProductController::class, 'listGroups'])->middleware('auth:sanctum');
Route::get('/products/list', [ProductController::class, 'listProducts'])->middleware('auth:sanctum');
Route::post('/products/groups/create', [ProductController::class, 'createGroup'])->middleware('auth:sanctum');
Route::post('/products/groups/delete', [ProductController::class, 'deleteGroup'])->middleware('auth:sanctum');
Route::post('/products/groups/edit', [ProductController::class, 'editGroup'])->middleware('auth:sanctum');

Route::get('/products/categories/list', [ProductController::class, 'listCategories'])->middleware('auth:sanctum');
Route::post('/products/categories/create', [ProductController::class, 'createCategory'])->middleware('auth:sanctum');
Route::post('/products/categories/edit', [ProductController::class, 'editCategory'])->middleware('auth:sanctum');
Route::post('/products/categories/remove', [ProductController::class, 'removeCategory'])->middleware('auth:sanctum');
Route::post('/products/sub-categories/create', [ProductController::class, 'createSubCategory'])->middleware('auth:sanctum');
Route::post('/products/create', [ProductController::class, 'store'])->middleware('auth:sanctum');

// Route::post('/products/edit', [ProductController::class, 'edit'])->middleware('auth:sanctum');
// Route::post('/products/remove/picture', [ProductController::class, 'removePicture'])->middleware('auth:sanctum');
Route::post('/products/delete', [ProductController::class, 'delete'])->middleware('auth:sanctum');
Route::post('/products/archive', [ProductController::class, 'toggleArchive'])->middleware('auth:sanctum');
Route::post('/products/edit-list-price', [ProductController::class, 'editListPrice'])->middleware('auth:sanctum');
Route::get('/products/all/list', [ProductController::class, 'listAll'])->middleware('auth:sanctum');
Route::get('/products/archived/list', [ProductController::class, 'listArchived'])->middleware('auth:sanctum');
Route::get('/products/{group}/list', [ProductController::class, 'list'])->middleware('auth:sanctum');

Route::get('/products/get/{id}', [ProductController::class, 'find'])->middleware('auth:sanctum');
Route::post('/products/gallery/sort', [ProductController::class, 'sortProductGallery'])->middleware('auth:sanctum');
Route::post('/products/gallery/set', [ProductController::class, 'setProductGallery'])->middleware('auth:sanctum');
Route::get('/products/gallery/get/{product_id}', [ProductController::class, 'getProductGallery'])->middleware('auth:sanctum');



//-----------------------------------------------------------------------------------------
//  ORDERS
//-----------------------------------------------------------------------------------------

Route::post('/orders/create', [OrderController::class, 'store']);


//-----------------------------------------------------------------------------------------
//  PAYMENT METHODS
//-----------------------------------------------------------------------------------------

Route::post('/payment_method/create', [PaymentMethodController::class, 'createMethod'])->middleware('auth:sanctum');
Route::get('/payment_method/list', [PaymentMethodController::class, 'listMethods'])->middleware('auth:sanctum');
Route::post('/payment_method/delete', [PaymentMethodController::class, 'deleteMethod'])->middleware('auth:sanctum');

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------

Route::post('/check-username-email', [AuthController::class, 'checkUsernameEmail']);

Route::post('/restore-password', [AuthController::class, 'restorePassword']);

Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
Route::post('/complete-register', [AuthController::class, 'completeRegister'])->middleware('auth:sanctum');

Route::post('/delete-account', [AuthController::class, 'deleteAccount'])->middleware('auth:sanctum');
Route::post('/send-delete-mail', [AuthController::class, 'sendDeleteMail'])->middleware('auth:sanctum');
Route::post('/delete-accout-message', [AuthController::class, 'deleteAccounMessage']);

//-----------------------------------------------------------------------------------------
//  POLICIES
//-----------------------------------------------------------------------------------------

Route::post('/policy-update', [PolicyController::class, 'updatePolicy'])->name('api.policy-update');
Route::post('/privacy-update', [PrivacyController::class, 'updatePrivacy'])->name('api.privacy-update');
Route::post('/data_deletion-update', [DataDeletionController::class, 'updateDataDeletion'])->name('api.data_deletion-update');

//-----------------------------------------------------------------------------------------
//  Calendly
//-----------------------------------------------------------------------------------------

Route::post('/calendly-update', [CalendlyKeyController::class, 'updateCalendly'])->middleware('auth:sanctum');

Route::get('/calendly-get', [CalendlyKeyController::class, 'getCalendly'])->middleware('auth:sanctum');

Route::post('/calendly-delete', [CalendlyKeyController::class, 'deleteCalendly'])->middleware('auth:sanctum');


