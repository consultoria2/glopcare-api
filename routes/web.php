<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use App\Mail\UserWelcome;
use App\Models\DataDeletion;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use App\Models\Policy;
use App\Models\Privacy;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('policy', function () {
    $policy = Policy::where('id', 1)->first();
    return view('policy', ['policy' => $policy]);
});

Route::get('privacy', function () {
    $privacy = Privacy::where('id', 1)->first();
    return view('privacy', ['privacy' => $privacy]);
});

Route::get('data-deletions', function () {
    $data_deletions = DataDeletion::where('id', 1)->first();
    return view('data-deletions', ['data_deletions' => $data_deletions]);
});

Route::post('policy-update', [PolicyController::class, 'updatePolicy'])->name('policy-update');
Route::post('privacy-update', [PrivacyController::class, 'updatePrivacy'])->name('privacy-update');

Route::get('delete-account', function () {
    return view('delete-account');
});

Route::get('/verified-page', function (Request $request) {
    $code = $request->query('code');
    return view('verified-page', ['code' => $code]);
})->name('verified-page');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


// Route::group(['middleware' => ['auth:sanctum']], function() {
//     Route::resource('users', UserController::class);
// });

Route::get('/users', function () {
    $selected = [];
    return view('users', ['selected' => $selected]);
})->middleware('auth:sanctum')->name('users.index');

Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::resource('orders', OrderController::class);
});

Route::get('/order/{orderId}', function ($orderId) {
    $order = Order::find($orderId);
    return view('order', ['order' => $order]);
})->middleware('auth:sanctum')->name('order.detail');

Route::get('/templates/users/{templateId}', function ($templateId) {
    $templateUsers = DB::table('glopcard_user_template')->where('template_id', $templateId)->get();
    $selected = array_map('strval', DB::table('glopcard_user_template')->where('template_id', $templateId)->get()->pluck('user_id')->toArray());

    return view('template-users', ['templateId' => $templateId, 'templateUsers' => $templateUsers, 'selected' => $selected]);
})->middleware('auth:sanctum')->name('template-users.detail');

Route::get('/order/{status}/{orderId}', [OrderController::class, 'setStatus'])->middleware('auth:sanctum')->name('order.status');

/* Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::resource('templates', TemplateController::class);
}); */

Route::group(['middleware' => ['auth:sanctum', 'checkUserId']], function() {
    Route::resource('templates', TemplateController::class);
});

Route::group(['middleware' => ['auth:sanctum', 'checkUserId']], function() {
    Route::resource('posts', PostsController::class);
});


Route::get('/demo', function () {
    return new UserWelcome();
});

Route::get('/password/reset/{token}', function ($token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::get('/{glopcard}', [DynamicLinksController::class, 'previewCard']);

// Route::get('/{glopcard}', function ($glopcardId) {

// });