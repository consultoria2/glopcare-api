<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Template extends Model
{
    use HasFactory;

    protected $table = 'glopcard_templates';

    protected $fillable = ['name','description','is_active','preview_uri','template','style','is_active'];

    // protected $appends = [
    //     'glopcards',
    // ];

    // public function glopcards() {
    //     return DB::table('glopcard')->where('user_id', $this->id)->where('is_active', true)->get();
    // }
}
