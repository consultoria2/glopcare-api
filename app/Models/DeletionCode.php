<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeletionCode extends Model
{
    use HasFactory;   
    protected $table = 'deletions_codes';

    protected $fillable = [
        'code',
        'user_id',
        'expiration_time'
    ];
}
