<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendlyKey extends Model
{
    use HasFactory;
    protected $table = 'calendly_keys';

    protected $fillable = [
        'calendly_url',
       // 'calendly_key',
        'calendly_secret',
        'user_id',
    ];
}
