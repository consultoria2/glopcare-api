<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Glopcard extends Model
{
    use HasFactory;

    protected $table = 'glopcard';

    protected $primaryKey = 'uid';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = ['user_id','snapshot','is_active', 'views', 'template_id'];
}
