<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeleteAccountMessage extends Model
{
    use HasFactory;
    protected $table = 'delete_account_messages';

    protected $fillable = [
        'message',
        'email',
        'name',
    ];
}
