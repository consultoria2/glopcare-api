<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class OrderLines extends Model
{
    protected $fillable = ['order_id', 'product_id', 'qty', 'net_price', 'net_amount', 'discount_amount', 'tax_amount', 'comments'];

    protected $with = ['product'];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
