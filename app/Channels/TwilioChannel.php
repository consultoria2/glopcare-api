<?php
namespace App\Channels;

use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;

class TwilioChannel
{
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toTwilio($notifiable);


        $to = $notifiable->routeNotificationFor('Twilio');
        $from = env('TWILIO_FROM');
        
        $sid = env('TWILIO_ACCOUNT_SID');
        $token = env('TWILIO_AUTH_TOKEN');
        $msid = env('TWILIO_MSG_SERVICE_ID');

        $twilio = new Client($sid, $token);


        return $twilio->messages->create($to, [
            // "messagingServiceSid" => $msid,
            "from" => $from,
            "body" => $message->content
        ]);
    }
}