<?php

namespace App\Notifications;

use App\Channels\Messages\TwilioMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\TwilioChannel;

class VerifiedUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if (filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            array_push($channels, 'mail');
        }

        if (filter_var($this->user->phone, FILTER_SANITIZE_NUMBER_INT)) {
            array_push($channels, TwilioChannel::class);
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('¡Bienvenido a glopcare! - Gracias por verificar tu cuenta')
                    ->greeting('¡Bienvenido a glopcare!')
                    ->line('¡Hola, ' . $this->user->name . '!')
                    ->line('Gracias por verificar tu cuenta.') 
                    ->line('¡Estás tan sólo a un click de construir la mejor versión de tu imagen online!') 
                    ->action('Ir a glopcare', url('/'))
                    ->line('Si tienes alguna duda, comunícate con nuestros asesores, que te brindarán todo el soporte que necesitas para diseñar una impactante imagen online, adaptada a tus necesidades, gustos y preferencias.')
                    ->line('<strong>Somos glopcare</strong>')
                    ->line('La nueva forma de acercarte a tus clientes');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content('[glopcare] Verificaste satisfactoriamente tu cuenta.');
    }
}
