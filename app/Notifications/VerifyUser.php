<?php

namespace App\Notifications;

use App\Channels\Messages\TwilioMessage;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\TwilioChannel;

// https://www.twilio.com/blog/send-emails-laravel-8-gmail-smtp-server

class VerifyUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $password = null)
    {
        //
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if (filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            array_push($channels, 'mail');
        }

        if (filter_var($this->user->phone, FILTER_SANITIZE_NUMBER_INT)) {
            array_push($channels, TwilioChannel::class);
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('¡Bienvenido a glopcare! - Verifica tu cuenta')
            ->greeting('¡Hola!')
            ->greeting($this->user->name)
            ->line('<strong>¡Bienvenid@ a glopcare!</strong>')
            ->line('<strong>¡NO ESPERES MÁS!</strong>')
            ->line('<strong>Registra tu información profesional y diseña la pantalla de inicio de tu nueva e impactante imagen online.</strong>')
            ->line('Para confirmar tu correo, por favor haz click aquí:')
            ->action('Confirmar Correo', url('/api/verify/' . $this->user->verification_token))
            ->line('Glopcare Tu Contacto Interactivo, pensando en tí, pone a tu disposición la más innovadora APP para emprendedores, que impulsará tu imagen online o la de tu negocio, y queremos que pruebes nuestra sección “Pantalla de Inicio” que podrás personalizar con imágenes y colores de marca, en la cual tus clientes podrán:')
            ->line('1. Saber quién eres.')
            ->line('2. Describir a qué te dedicas.')
            ->line('3. Interactuar contigo a través de los canales de comunicación que registres (teléfono, mensajes de texto, WhatsApp, página web, redes sociales, etc.).')
            ->line('Además, podrás mostrar publicidades y promociones de los servicios y/o productos que ofreces, a través de un enlace de la sección de catálogo, que próximamente estará disponible.')
            ->line('Para saber más sobre glopcare comunícate con nuestro equipo de soporte, que está siempre listo para ayudarte a formar parte de esta exclusiva forma de interactuar con tus clientes y permitir que reconozcan tu imagen online donde quieras que la presentes, logrando así diferenciarte de tu competencia.')
            ->line('<strong>Somos glopcare</strong>')
            ->salutation('La nueva forma de acercarte a tus clientes');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content('[glopcare] Verifica tu cuenta con el siguiente link ' . url('/api/verify/' . $this->user->verification_token));
    }
}
