<?php

namespace App\Notifications;

use App\Channels\Messages\TwilioMessage;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\TwilioChannel;

// https://www.twilio.com/blog/send-emails-laravel-8-gmail-smtp-server

class AccountDeleted extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if (filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            array_push($channels, 'mail');
        }

        if (filter_var($this->user->phone, FILTER_SANITIZE_NUMBER_INT)) {
            array_push($channels, TwilioChannel::class);
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('¡Hasta Pronto!')
            ->greeting('¡Hola ' . $this->user->name . '!')
            ->line('<strong>Este es un mensaje de confirmación por haber eliminado tu cuenta glopcare.</strong>')
            ->line('Lamentamos tu decisión de haber eliminado tu cuenta definitivamente. Para nosotros tu opinión es muy importante y te agradeceríamos que nos informaras los motivos por los cuales eliminaste tu cuenta, <strong>con la finalidad de mejorar nuestros servicios</strong>. Para tu comodidad, puedes responder este mensaje por esta misma vía o comunicarte con nuestro equipo de soporte que siempre está dispuesto a ayudarte.')
            ->line('Si crees que esto es un error comunícate con atencioalcliente@glopcare.com')
            ->line('Saludos')
            ->line('<strong>Somos tu equipo glopcare</strong>')
            ->salutation('La automatización a tu alcance');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content('[glopcare] Eliminamos los datos de tu cuenta porque así lo has solicitado desde tu app glopcare');
    }
}
