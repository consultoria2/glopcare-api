<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\TwilioChannel;
use App\Channels\Messages\TwilioMessage;

class PasswordReset extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        //
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {

        $channels = [];

        if (filter_var($notifiable->email, FILTER_VALIDATE_EMAIL)) {
            array_push($channels, 'mail');
        }

        if (filter_var($notifiable->phone, FILTER_SANITIZE_NUMBER_INT)) {
            array_push($channels, TwilioChannel::class);
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Solicitaste Resetear Contraseña')
            ->greeting('Sigue estas instrucciones para continuar el reseteo de la contraseña')
            ->line('Estas recibiendo este correo porque recibimos una solicitud de reestablecimiento de contraseña de tu cuenta.') // Here are the lines you can safely override
            ->action('Resetear Contraseña', url('password/reset', $this->token))
            ->line('Si lo hiciste por error, puedes ignorar este mensaje.')
            ->line('Si no lo solicitaste, notifica a atencionalcliente@glopcare.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content('[glopcare] Resetea tu contraseña visitando ' . url('password/reset', $this->token));
    }
}
