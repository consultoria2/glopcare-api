<?php

namespace App\Notifications;

use App\Mail\OrderDetail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewOrder extends Notification
{
    use Queueable;

    public $order;
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $user)
    {
        //
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [];

        if (filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            array_push($channels, 'mail');
        }

        if (filter_var($this->user->phone, FILTER_SANITIZE_NUMBER_INT)) {
            array_push($channels, TwilioChannel::class);
        }

        return $channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new OrderDetail($this->user, $this->order))
                ->to($notifiable->email);
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioMessage)
            ->content('[glopcare] Se ha creado un nuevo pedido en Glopcare');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
