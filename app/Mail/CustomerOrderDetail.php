<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerOrderDetail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $order;
    public $payment_methods;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $order, $payment_methods)
    {
        //
        $this->customer = $customer;
        $this->order = $order;
        $this->payment_methods = $payment_methods;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('¡Nuevo Pedido!')
                ->markdown('emails.order.detail')
                ->with('greeting', '¡Gracias por preferirnos!')
                ->with('introLines', [
                    '¡Hola, ' . $this->customer->name . '!', 'Se ha creado el pedido #' . $this->order->store_order_number,
                    'Hemos recibido tu solicitud para obtener lo(s) siguiente(s):'
                    ])
                ->with('outroLines', [
                    'NOTA: La disponibilidad y los precios pueden variar sin previo aviso, nos comunicaremos contigo a la mayor brevedad posible para confirmar tu(s) solicitud(es)',
                    'N/A: Información no disponible'
                    ])
                ->with('customer', $this->customer)
                ->with('order', $this->order)
                ->with('payment_methods', $this->payment_methods);
    }
}
