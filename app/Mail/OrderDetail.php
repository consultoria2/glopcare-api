<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderDetail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $order)
    {
        //
        $this->customer = $customer;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('¡Nuevo Pedido!')
                ->markdown('emails.order.detail')
                ->with('greeting', '¡Nuevo Pedido!')
                ->with('introLines', ['¡Hola, ' . $this->customer->name . '!', 'Se ha creado el pedido #' . $this->order->store_order_number . '. Podrás revisar los detalles en tu App Glopcare'])
                ->with('outroLines', [])
                ->with('customer', $this->customer)
                ->with('order', $this->order);
    }
}
