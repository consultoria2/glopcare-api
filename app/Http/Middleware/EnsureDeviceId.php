<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;

class EnsureDeviceId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        if (isset($request['device_id']) && !is_null($request['device_id']) && !is_null($user->last_device_id) && $request['device_id'] != $user->last_device_id) {
            return response()->json([
                'message' => 'Tu sesión ha sido abierta en otro dispositivo. Si consideras que esto es un error, contacta a tu equipo Glopcare. Para garantizar la integridad de la información, cerraremos tu sesión en este dispositivo.'
              ], 403);
        }

        return $next($request);
    }
}
