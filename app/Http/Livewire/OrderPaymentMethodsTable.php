<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\OrderPaymentMethods;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class OrderPaymentMethodsTable  extends LivewireDatatable
{
    public $model = OrderPaymentMethods::class;
    public $orderId;

    public function builder()
    {
        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $query = OrderPaymentMethods::with('paymentMethod')->where('order_id', $this->orderId);
        } else {
            $order = Order::where('store_id', auth()->user()->id)->get()->first();
            if (!is_null($order)) {
                $query = OrderPaymentMethods::with('paymentMethod')->where('order_id', $this->orderId);
            }
        }
        
        return $query;
    }

    public function columns()
    {
        return [
            Column::name('paymentMethod.name')->label('Instrumento de Pago')->searchable(),
            Column::name('confirmation_number')->label('Número de Confirmación'),
            Column::name('amount')->label('Monto')->enableSummary()
        ];
    }
}
