<?php

namespace App\Http\Livewire;

use App\Http\Controllers\TemplateController;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Action;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\DB;

class TemplateUsersTable extends LivewireDatatable
{
    public $model = User::class;
    public $selected = [];
    public $templateId;


    public function builder()
    {
        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $query = User::query();
        } else {
            $query = User::query()->where('id', auth()->user()->id);
        }
        return $query;
    }


    public function columns()
    {
        return [
            Column::checkbox(),
            Column::name('id'),
            Column::name('name')->searchable(),
            Column::name('email')->searchable(),
            Column::name('created_at')
        ];
    }
    

    public function buildActions()
    {
        return [

            Action::value('assign')->label('Asignar')->group('Acciones')->callback(function ($mode, $items) {
                // $items contains an array with the primary keys of the selected items
                $controller = new TemplateController();
                $controller->assign($this->templateId, $items);
            }),

            Action::value('release')->label('Liberar')->group('Acciones')->callback(function ($mode, $items) {
                $controller = new TemplateController();
                $controller->release($this->templateId, $items);
                $this->selected = array_map('strval', DB::table('glopcard_user_template')->where('template_id', $this->templateId)->get()->pluck('user_id')->toArray());
            }),

            // Action::groupBy('Export Options', function () {
            //     return [
            //         Action::value('csv')->label('Export CSV')->export('SalesOrders.csv'),
            //         Action::value('html')->label('Export HTML')->export('SalesOrders.html'),
            //         Action::value('xlsx')->label('Export XLSX')->export('SalesOrders.xlsx')->styles($this->exportStyles)->widths($this->exportWidths)
            //     ];
            // }),
        ];
    }
}