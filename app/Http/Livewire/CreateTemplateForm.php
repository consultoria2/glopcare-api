<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CreateTemplateForm extends Component
{
    public function render()
    {
        return view('livewire.create-template-form');
    }
}
