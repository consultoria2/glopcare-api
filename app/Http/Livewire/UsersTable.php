<?php

namespace App\Http\Livewire;

use App\Http\Controllers\UserController;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Action;

class UsersTable extends LivewireDatatable
{
    public $model = User::class;
    public $selected = [];

    public function builder()
    {

        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $query = User::query();
        } else {
            $query = User::query()->where('id', auth()->user()->id);
        }
        return $query;
    }

    public function columns()
    {
        return [
            Column::checkbox(),
            Column::name('id'),
            Column::name('name')->searchable(),
            Column::name('email')->searchable(),
            Column::callback(['last_device_id'], function ($last_device_id) {
                return $last_device_id;
            })->label('Sesión de Usuario')->searchable(),
            Column::name('created_at'),
            Column::delete()
        ];
    }

    public function buildActions()
    {
        return [

            Action::value('release')->label('Liberar Sesión')->group('Acciones')->callback(function ($mode, $items) {
                $controller = new UserController();
                $controller->release($items);
                $this->selected = [];
            }),
        ];
    }
}