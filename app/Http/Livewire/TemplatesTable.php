<?php

namespace App\Http\Livewire;

use App\Models\Template;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\DB;

class TemplatesTable extends LivewireDatatable
{
    public $model = Template::class;

    public function builder()
    {
        return Template::query();
    }

    public function columns()
    {
        return [
            Column::name('id')->link('/templates/users/{{id}}', 'Visibilidad'),
            Column::name('name'),
            NumberColumn::callback(['id'], function ($id) {
                return DB::table('glopcard')->where('template_id', $id)->where('is_active', true)->get()->count();
            })->label('Active Glopcards'),
            Column::name('is_active'),
            Column::name('created_at'),
            Column::delete()
        ];
    }
}