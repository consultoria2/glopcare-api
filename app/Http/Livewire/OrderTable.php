<?php

namespace App\Http\Livewire;

use App\Models\OrderLines;
use App\Models\Order;
use App\Models\Product;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class OrderTable extends LivewireDatatable
{
    public $model = OrderLines::class;
    public $orderId;

    public function builder()
    {
        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $query = OrderLines::query()->where('order_id', $this->orderId);
        } else {
            $order = Order::where('store_id', auth()->user()->id)->get()->first();
            if (!is_null($order)) {
                $query = OrderLines::query()->where('order_id', $this->orderId);
            }
        }
        return $query;
    }

    public function columns()
    {
        return [
            Column::callback(['product_id'], function ($product_id) {
                $product = Product::find($product_id);
                return !is_null($product) ? $product->product_name : 'Producto Eliminado';
            })->label('Producto o Servicio')->searchable(),
            Column::name('qty')->label('Cantidad'),
            Column::name('net_price')->label('Precio'),
            Column::name('net_amount')->label('Monto Neto')->enableSummary()
        ];
    }
}