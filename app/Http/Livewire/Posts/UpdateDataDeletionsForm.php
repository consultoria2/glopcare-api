<?php

namespace App\Http\Livewire\Posts;

use App\Models\DataDeletion;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdateDataDeletionsForm extends Component
{
    use WithFileUploads;

    public $current_data_deletion;
    protected $rules = [
        'data_deletions_content' => 'required',
    ];

    public function mount()
    {        
        $this->current_data_deletion = DataDeletion::where('id', 1)->first();
    }

    public function render()
    {
        return view('livewire.posts.update-data-deletions-form');
    }
}
