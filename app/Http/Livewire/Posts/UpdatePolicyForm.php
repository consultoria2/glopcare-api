<?php

namespace App\Http\Livewire\Posts;

use App\Models\Policy;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdatePolicyForm extends Component
{
    use WithFileUploads;

    public $current_policy;
    protected $rules = [
        'policies_content' => 'required',
    ];

    public function mount()
    {        
        $this->current_policy = Policy::where('id', 1)->first();
    }



    public function render()
    {
        return view('livewire.posts.update-policy-form');
    }
}

