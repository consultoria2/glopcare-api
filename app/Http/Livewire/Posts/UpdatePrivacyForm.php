<?php

namespace App\Http\Livewire\Posts;

use App\Models\Privacy;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdatePrivacyForm extends Component
{
    use WithFileUploads;

    public $current_privacy;
    protected $rules = [
        'privacies_content' => 'required',
    ];

    public function mount()
    {        
        $this->current_privacy = Privacy::where('id', 1)->first();
    }



    public function render()
    {
        return view('livewire.posts.update-privacy-form');
    }
}

