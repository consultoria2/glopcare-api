<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class CreateUserForm extends Component
{
    public $username;
    public $name;
    public $email;
    public $password;

    public function submit()
    {
        $validatedData = $this->validate([
            'username' => 'required|min:6',
            'name' => 'required|min:6',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        User::create($validatedData);

        return redirect()->to('/users');
    }

    public function render()
    {
        return view('livewire.create-user-form');
    }
}
