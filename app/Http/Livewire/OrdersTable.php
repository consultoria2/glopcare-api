<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class OrdersTable extends LivewireDatatable
{
    public $model = Order::class;

    public function builder()
    {
        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $query = Order::query();
        } else {
            $query = Order::query()->where('store_id', auth()->user()->id);
        }
        return $query;
    }

    public function columns()
    {
        return [
            Column::callback(['store_id'], function ($store_id) {
                $user = User::find($store_id);
                return !is_null($user) ? $user->username : 'Usuario Eliminado';
            })->label('Glopcard'),
            Column::name('id')->link('/order/{{id}}', 'Ver'),
            Column::name('store_order_number')->label('Pedido')->searchable(),
            Column::name('customer_name')->label('Nombre Suministrado')->searchable(),
            Column::callback(['customer_phone'], function ($phone) {
                return '<a href="https://wa.me/'.$phone.'" target="_blank">'.$phone.'</a>';
            })->label('Teléfono Suministrado')->searchable(),
            Column::name('customer_email')->label('Correo Suministrado')->searchable(),
            Column::name('shipping_address')->label('Dirección Suministrado')->searchable(),
            Column::name('total_amount')->label('Monto Total'),
            Column::name('total_lines')->label('Cantidad de Lineas'),
            Column::name('created_at')->label('Creada'),
            Column::name('updated_at')->label('Actualizada'),
            Column::name('status')->searchable()
        ];
    }
}