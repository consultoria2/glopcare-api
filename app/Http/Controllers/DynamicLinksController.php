<?php

namespace App\Http\Controllers;

use App\Models\Glopcard;
use App\Models\User;

class DynamicLinksController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function previewCard($glopcardId)
    {
        $glopcard = Glopcard::where('uid', $glopcardId)->where('is_active', true)->get()->first();

        if (!is_null($glopcard)) {
            return redirect()->away('https://glopcard.glopcare.com/' . $glopcard->uid);
        } else {
            $user = User::where('username', $glopcardId)->where('status', 'verified')->get()->first();

            if (!is_null($user)) {
                $glopcard = Glopcard::where('user_id', $user->id)->where('is_active', true)->get()->first();

                if (!is_null($glopcard)){
                    $snapshot = json_decode($glopcard->snapshot);
                    $avatar = str_replace('")', '', str_replace('url("', '', $snapshot->selected_template->style->templateAvatar->background));
                    $avatar = preg_replace('#^data:image/\w+;base64,#i', '', $avatar);
    
                    $safeName = $glopcard->uid . '.' .'png';
                    $success = file_put_contents(public_path() . '/storage/uploads/' . $safeName, base64_decode($avatar));
    
                    return view('dynamics')
                        ->with('previewType', 'glopcard')
                        ->with('user', $user)
                        ->with('snapshot', $snapshot)
                        ->with('avatar', $safeName)
                        ->with('previewData', $glopcard)
                        ->with('isCrawler', $this->isCrawler());
                } else {
                    abort(404);
                    // return redirect()->away('https://glopcard.glopcare.com/' . $glopcard->uid);
                }
            }
        }

        return view('welcome');
    }

    /**
     * Checks if the user-agent of the request is a crawler bot
     */
    public function isCrawler()
    {
        return stripos($_SERVER['HTTP_USER_AGENT'], 'googlebot')
            || stripos($_SERVER['HTTP_USER_AGENT'], 'google-structured-data-testing-tool')
            || stripos($_SERVER['HTTP_USER_AGENT'], 'bingbot')
            || stripos($_SERVER['HTTP_USER_AGENT'], 'linkedinbot')
            || stripos($_SERVER['HTTP_USER_AGENT'], 'mediapartners-google');
    }

    // public function previewDisc($id)
    // {
    //     $course = Course::find($id);
    //     return view('dynamics')->with('previewType', 'disc')->with('previewData', $profile);
    // }
}
