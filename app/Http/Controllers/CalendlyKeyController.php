<?php

namespace App\Http\Controllers;

use App\Models\CalendlyKey;
use App\Models\User;
use Illuminate\Http\Request;

class CalendlyKeyController extends Controller
{
    public function updateCalendly(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $data = $request->all();

        $calendlyKey = CalendlyKey::updateOrCreate([
            'user_id' =>  $user->id,
            'id' => $data['id'],
        ], [
            'calendly_url' => $data['calendly_url'],
          //  'calendly_key' => $data['calendly_key'],
            'calendly_secret' => $data['calendly_secret']
        ]);

        return response()->json($calendlyKey);
    }

    public function getCalendly(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $calendlyKey = CalendlyKey::where('user_id', $user->id)->first();

        return response()->json($calendlyKey);
    }

    public function deleteCalendly(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $calendly = $request->all();

        $calendlyKey = CalendlyKey::where('user_id', $user->id)
            ->where('id', $calendly['id'])
            ->first();

        if (is_null($calendlyKey)) {
            return response()->json([
                'message' => 'Calendly key not found'
            ], 404);
        }

        $calendlyKey->delete();

        return response()->json([
            'message' => 'Calendly key deleted successfully'
        ], 200);
    }

    
}
