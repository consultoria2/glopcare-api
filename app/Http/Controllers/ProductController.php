<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Store;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use ImageOptimizer;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    public static function listGroups($user_id = null)
    {
        $user = User::find($user_id ? $user_id : auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        // $groups = DB::select(DB::raw(
        //     "select pg.id, pg.name, pg.is_active, (select count(*) from products p where p.group_id = pg.id and archived = false) as product_count from product_group pg where pg.user_id = " . $user->id .
        //         " union all " .
        //         "select null, 'Grupo Único', 1, (select count(*) from products p where p.group_id is null and p.user_id = " . $user->id . " and archived = false) as product_count from dual"
        // ));

        $groups = DB::select(DB::raw(
            "select pg.id, pg.name, pg.is_active, (select count(*) from products p where p.group_id = pg.id and archived = false) as product_count from product_group pg where pg.user_id = " . $user->id
        ));

        $product_groups = array();

        foreach ($groups as $group) {
            array_push($product_groups, $group);
        }

        return $product_groups;
    }

    public function listProducts()
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $products = DB::select(DB::raw("select p.* from products p where p.user_id = " . $user->id));

        return $products;
    }

    public function createGroup(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        $group = DB::table('product_group')->insertGetId([
            'user_id' => $user->id,
            'name' => $element['name'],
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return $group;
    }

    public function deleteGroup(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        $group = DB::table('product_group')->where('id', $element['id'])->where('user_id', $user->id)->get()->first();

        if (is_null($group)) {
            $response = [
                'error' => 'Forbidden',
                'message' => 'The group does not belong to the user'
            ];

            return response()->json($response, 403);
        }

        $group = DB::table('products')->where('group_id', $element['id'])->where('user_id', $user->id)->get()->first();

        if (!is_null($group)) {
            $response = [
                'error' => 'Forbidden',
                'message' => 'The group has products that should be reorganized'
            ];

            return response()->json($response, 403);
        }

        $group = DB::table('product_group')->where('id', $element['id'])->where('user_id', $user->id)->delete();

        return response()->json($group, 200);
    }

    public function editGroup(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        $group = DB::table('product_group')->where('user_id', $user->id)->where('id', $element['id'])->update([
            'name' => $element['name'],
            'is_active' => true,
            'updated_at' => Carbon::now(),
        ]);

        return $group;
    }    

    public static function listCategories($user_id = null)
    {
        $user = User::find($user_id ? $user_id : auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $categories = DB::select(DB::raw("select c.id, c.category_name, c.is_active, c.parent_category from categories c where c.user_id = " . $user->id . " order by c.category_name"));

        $product_categories = array();

        foreach ($categories as $category) {
            array_push($product_categories, $category);
        }

        return $product_categories;
    }

    public function createCategory(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        $category = DB::table('categories')->insertGetId([
            'user_id' => $user->id,
            'category_name' => $element['category_name'],
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return $category;
    }

    public function editCategory(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        $category = DB::table('categories')->where('user_id', $user->id)->where('id', $element['category_id'])->update([
            'category_name' => $element['category_name'],
            'is_active' => true,
            'updated_at' => Carbon::now(),
        ]);

        return $category;
    }

    public function removeCategory(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        if (is_null($element['parent_category']) || $element['parent_category'] == null || $element['parent_category'] == 'null') {
            // Parent category is null so their children must be deleted

            // Obtaining child categories
            $child_categories = DB::table('categories')->where('parent_category', $element['category_id'])->get();

            // Removing child categories
            foreach($child_categories as $child){
                $product_categories = DB::table('product_category')->where('category_id', $child->id)->delete();
                $categories = DB::table('categories')->where('id', $child->id)->delete();
            }

            // Removing parent category
            $product_categories = DB::table('product_category')->where('category_id', $element['category_id'])->delete();
        } else {
            $product_categories = DB::table('product_category')->where('category_id', $element['category_id'])->update([
                'category_id' => $element['parent_category'],
                'updated_at' => Carbon::now(),
            ]);
        }

        $category = DB::table('categories')->where('user_id', $user->id)->where('id', $element['category_id'])->delete();

        return $category;
    }

    public function createSubCategory(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $element = $request->all();

        if (!isset($element['parent_category']) || !is_numeric($element['parent_category'])) {
            return response()->json([
                'message' => 'Category was not set'
            ], 401);
        } else {
            $checkCategory = DB::table('categories')->where('user_id', $user->id)->where('id', $element['parent_category'])->get()->first();

            if (is_null($checkCategory)) {
                return response()->json([
                    'message' => 'Category not found'
                ], 401);
            }
        }

        $subCategory = DB::table('categories')->insertGetId([
            'user_id' => $user->id,
            'category_name' => $element['category_name'],
            'parent_category' => $element['parent_category'],
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return $subCategory;
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'product_name'          => 'required|string',
        //     'price'                 => 'required|numeric',
        //     'price_type'            => 'required|string',
        //     'product_type'          => 'required|string',
        //     'description'           => 'required|string',
        //     'product_categories'    => 'required'
        // ]);

        $product_data = $request->all();

        // If group is missing, find first group

        if (!$request->group_id || $request->group_id == 'null' || $request->group_id == 'undefined') {
            $product_data['group_id'] = null;
        } else {
            $group = DB::table('product_group')->where('name', $product_data['group_id'])->where('user_id', auth()->user()->id)->first();

            if (is_null($group)) {
                $product_data['group_id'] = null;
            } else {
                $product_data['group_id'] = $group->id;
            }
        }


        // if (!$request->product_code || $request->product_code == 'null' || $request->product_code == 'undefined') {
        //     $product_data['product_code'] = $request->product_name;
        // }

        // if (!$request->discount_price || $request->discount_price == 'null' || $request->discount_price == 'undefined') {
        //     $product_data['discount_price'] = $request->price;
        // }

        // $product_images = $request->input('product_images.*');

        // if ($product_images && count($product_images) > 0) {
        //     $product_data['main_image'] = '*';
        // }

        // $product_data['tax_id'] = !is_null($request->tax_id) ? $request->tax_id : env('DEFAULT_TAX_ID', 0);

        // Generating default data
        $product_data['availability'] = true;

        $product_data['stock'] = 1;
        $product_data['user_id'] = auth()->user()->id;


        if (isset($product_data['id']) && !is_null($product_data['id'])) {
            $product = Product::find($product_data['id']);
            $product->fill($product_data);
            $product->save();
        } else {
            $product_data['product_code'] = uniqid();
            $product = Product::create($product_data);
        }



        // Validate product images

        // if ($product_images && count($product_images) > 0) {

        //     foreach ($product_images as $file) {
        //         $folderPath = "images/" . $store->id . "/";
        //         $image_parts = explode(";base64,", $file);
        //         $image_type_aux = explode("image/", $image_parts[0]);
        //         $image_type = $image_type_aux[1];
        //         $image_base64 = base64_decode($image_parts[1]);
        //         $name = $folderPath . uniqid() . '.' . $image_type;

        //         if (Storage::disk('public_uploads')->put($name, $image_base64)) {

        //             // Resize if image is too wide
        //             $img = Image::make(public_path() . '/uploads/' . $name);

        //             if ($img->width() > 1200) {
        //                 $img->resize(1200, null, function ($constraint) {
        //                     $constraint->aspectRatio();
        //                 });

        //                 $img->save(public_path() . '/uploads/' . $name);
        //             }


        //             // Optimize image
        //             ImageOptimizer::optimize(public_path() . '/uploads/' . $name);
        //             DB::table('product_images')->insert(['product_id' => $product->id, 'image' => $name]);

        //             if ($product_data['main_image']  == '*') {
        //                 $product_data['main_image']  = $name;
        //                 $product->main_image = $name;
        //                 $product->save();
        //             }
        //         } else {
        //             $response = [
        //                 'error' => 'File upload failed',
        //                 'message' => 'We could not upload your file'
        //             ];
        //             return response()->json($response, 401);
        //         }
        //     }
        // }

        $product_categories = $request->input('product_categories.*');

        DB::table('product_category')->where('product_id', $product->id)->delete();

        foreach ($product_categories as $category) {
            DB::table('product_category')->insert(['product_id' => $product->id, 'category_id' => $category['id'], 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        }

        return response()->json($product, 200);
    }

    public static function listAll($user_id = null)
    {
        $products = Product::where('user_id', ($user_id ? $user_id : auth()->user()->id))->where('archived', false)->get();
        return response()->json($products, 200);
    }

    public static function listArchived($user_id = null)
    {
        $products = Product::where('user_id', ($user_id ? $user_id : auth()->user()->id))->where('archived', true)->get();
        return response()->json($products, 200);
    }

    public function list($group)
    {

        $group = DB::table('product_group')->where('name', $group)->where('user_id', auth()->user()->id)->first();

        if (is_null($group)) {
            $products = Product::whereNull('group_id')->where('user_id', auth()->user()->id)->where('archived', false)->get();
        } else {
            $products = Product::whereRaw('(group_id = ? or group_id is null) and user_id = ? and archived = false', [$group->id, auth()->user()->id])->get();
        }

        return response()->json($products, 200);
    }

    public function find($id)
    {
        $product = Product::find($id);

        if (!$product) {
            $response = [
                'error' => 'Not found',
                'message' => 'The product was not found',
                'type' => 'info'
            ];

            return response()->json($response, 404);
        }

        return response()->json($product, 200);
    }

    public function sortProductGallery(Request $request)
    {
        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $gallery = json_decode($element['gallery']);

        foreach ($gallery as $image) {

            //Upsert group names

            if (property_exists($image, 'id')) {
                $imageId = $image->id;
            } else {
                return response()->json([
                    'message' => 'Can not sort because image id is not present',
                    'type' => 'info',
                ], 503);
            }

            $position = 0;

            if ($image->x == 0 && $image->y == 0) {
                $position = 1;
            }

            if ($image->x == 1 && $image->y == 0) {
                $position = 2;
            }

            if ($image->x == 0 && $image->y == 1) {
                $position = 3;
            }

            if ($image->x == 1 && $image->y == 1) {
                $position = 4;
            }

            DB::table('product_image')->where('id', $imageId)->update(['position' => $position, 'updated_at' => Carbon::now()]);
        }

        return response()->json([
            'message' => 'Glopcard product gallery updated',
            'type' => 'info',
        ], 200);
    }

    public function setProductGallery(Request $request)
    {

        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        if (!isset($element['product_id'])) {
            return response()->json([
                'message' => 'Product not found'
            ], 401);
        }

        $gallery = json_decode($element['catalog']);

        // Delete catalog images if IDs does not exist
        DB::table('product_image')->where('product_id', $element['product_id'])->whereNotIn('id', Arr::pluck($gallery, 'id'))->delete();

        $image_upserted = false;

        foreach ($gallery as $image) {

            //Upsert group images

            if (isset($image->image_binary)) {
                $image = $image->image_binary;

                $extension = 'png';

                if (strpos($image, 'image/jpeg')) {
                    $extension = 'jpg';
                    $image = str_replace('data:image/jpeg;base64,', '', $image);
                } else {
                    $image = str_replace('data:image/png;base64,', '', $image);
                }

                $image = str_replace(' ', '+', $image);
                $safeName = Str::uuid() . '.' . $extension;
                $success = file_put_contents(public_path() . '/storage/uploads/' . $safeName, base64_decode($image));
                ImageOptimizer::optimize(public_path() . '/storage/uploads/' . $safeName);
            } else {
                // Prevent uploading and deleting image
                $success = true;
                $safeName = $image->image_uri;
            }

            if ($success) {

                $position = 5;

                if (property_exists($image, 'position')) {
                    if ($image->position == 0) {
                        $position = 5;
                    } else {
                        $position = $image->position;
                    }
                }

                if (property_exists($image, 'id')) {
                    DB::table('product_image')->where('id', $image->id)->update(['image_uri' => $safeName, 'updated_at' => Carbon::now()]);
                    $image_upserted = true;
                } else {
                    DB::table('product_image')->insert(['image_uri' => $safeName, 'product_id' => $element['product_id'], 'position' => $position, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'user_id' => auth()->user()->id]);
                    $image_upserted = true;
                }
            } else {
                return response()->json([
                    'message' => 'Could not create file image',
                    'type' => 'info',
                ], 503);
            }
        }

        if ($image_upserted) {
            DB::table('products')->where('id', $element['product_id'])->update(['main_image' => '*', 'updated_at' => Carbon::now()]);
        } else {
            DB::table('products')->where('id', $element['product_id'])->update(['main_image' => null, 'updated_at' => Carbon::now()]);
        }


        return response()->json([
            'message' => 'Product gallery updated',
            'type' => 'info',
        ], 200);
    }

    public function getProductGallery($product_id)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }


        $toMapDomain = DB::table('product_image')->where('product_id', $product_id)->where('is_active', true)->orderBy('position')->get()->toArray();

        $arr_uri_domain = array();

        // Prepare for the frontend

        foreach ($toMapDomain as $key => $image) {
            $image->url = 'https://glopcare.com/storage/uploads/' . $image->image_uri;

            $nextY = intval($key / 2);
            $nextX = $key % 2;
            $nextI = $key;

            $image->x = $nextX;
            $image->y = $nextY;
            $image->w = 1;
            $image->h = 1;
            $image->i = $nextI;

            $style = new \stdClass;
            $style->backgroundSize = 'contain';
            $style->backgroundRepeat = 'no-repeat';
            $style->backgroundImage = 'url(' . $image->url . ')';

            $image->style = $style;

            array_push($arr_uri_domain, $image);
        }

        return $arr_uri_domain;
    }

    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------    

    public function listPublicOffers($limit = 0)
    {
        return $this->listOffers(null, $limit);
    }

    public function listStoreOffers($store, $limit = 0)
    {
        return $this->listOffers($store, $limit);
    }

    public function listOffers($store, $limit = 0)
    {
        $pivot = 0;

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $products = Product::where('availability', true)->where('archived', false)
                ->where('store_id', $store->id);
        } else {
            $products = Product::where('availability', true)->where('archived', false);
        }

        //Randomly order the result
        $products = $products->get()->shuffle();

        $arr_products = array();

        foreach ($products as $key => $product) {
            if ($product->discount_price && (floatval($product->discount_price) < floatval($product->price)) && $product->store->is_active == true) {

                unset($product->questions_and_answers);
                unset($product->categories);

                array_push($arr_products, $product);
                $pivot++;
            } else {
                $products->forget($key);
            }

            if ($limit > 0 && $limit == $pivot) break;
        }
        return response()->json($arr_products, 200);
    }

    public function search($query)
    {
        $arr_products = array();

        $stores = Store::where('is_active', true)->get()->pluck('id');

        if ($query == 'destacados') {
            $products = Product::where('availability', true)->where('archived', false)->where('starred', true)->whereIn('store_id', $stores)->get()->shuffle();
        } else {
            $products = Product::whereRaw("UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("id IN (SELECT p.product_id FROM product_category p WHERE p.category_id = (SELECT c.id FROM categories c WHERE UPPER(c.category_name) LIKE '%" . strtoupper($query) . "%' and c.is_active = 1))")->get();

            foreach ($products as $key => $product) {
                if (!$product->store->is_active) {
                    $products->forget($key);
                } else {
                    array_push($arr_products, $product);
                }
            }
        }

        return response()->json(count($arr_products) > 0 ? $arr_products : $products, 200);
    }

    public function storeSearch($store, $query, $limit = 0)
    {
        $store = Store::where('store_subdomain', $store)->get()->first();

        if ($query == "*") {
            $products = Product::whereRaw("store_id = " . $store->id)->where('availability', true)->where('archived', false)->get();
        } else {
            $products = Product::whereRaw("store_id = " . $store->id . " and UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and id IN (SELECT p.product_id FROM product_category p WHERE p.category_id in (SELECT c.id FROM categories c WHERE UPPER(c.category_name) LIKE '%" . strtoupper($query) . "%' and c.is_active = 1))")->get();
        }

        if ($limit > 0) {
            $category_count = array();
            foreach ($products as $key => $product) {

                foreach ($product->categories as $category_id) {
                    if (array_key_exists($category_id, $category_count)) {
                        $category_count[$category_id] = $category_count[$category_id] + 1;
                    } else {
                        $category_count[$category_id] = 0;
                    }

                    if ($category_count[$category_id] >= $limit) {
                        $products->forget($key);
                    }
                }
            }
        }

        return response()->json($products, 200);
    }

    public function storeCategorySearch($store, $query, $category)
    {
        $store = Store::where('store_subdomain', $store)->get()->first();

        if ($query == "*") {
            $products = Product::whereRaw("store_id = " . $store->id)->where('availability', true)->where('archived', false)->get();
        } else {
            $products = Product::whereRaw("store_id = " . $store->id . " and UPPER(product_name) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")
                ->orWhereRaw("store_id = " . $store->id . " and UPPER(description) LIKE '%" . strtoupper($query) . "%' and availability = 1 and archived = 0")->get();
        }

        $products = $products->filter(function ($value) use ($category) {
            return in_array($category, $value->categories);
        });

        foreach ($products as $product) {
            $product->category = $category;
        }

        return response()->json($products, 200);
    }

    public function edit(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'product_name' => 'required|string',
            'price' => 'required|numeric',
            'availability' => 'required',
            'stock' => 'required|numeric',
            'brand' => 'required|string',
            'description' => 'required|string',
            'product_code' => 'required|string',
            'product_category' => 'required'
        ]);

        $product_data = $request->all();

        if (!$request->store_id || $request->store_id == 'null' || $request->store_id == 'undefined') {
            $store = Store::where('user_id', auth()->user()->id)->first();
        } else {
            $store = Store::where('id', $request['store_id'])->where('user_id', auth()->user()->id)->first();
        }

        if (is_null($store)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended',
                'type' => 'info'
            ];
            return response()->json($response, 401);
        }


        // if (!$request->product_code || $request->product_code == 'null' || $request->product_code == 'undefined') {
        //     $product_data['product_code'] = $request->product_name;
        // }

        // if (!$request->discount_price || $request->discount_price == 'null' || $request->discount_price == 'undefined') {
        //     $product_data['discount_price'] = $request->price;
        // }

        // $product_images = $request->input('product_images.*');

        // if ($product_images && count($product_images) > 0) {
        //     $product_data['main_image'] = '*';
        // } else {
        //     $product_images = array();
        // }

        // $product_data['tax_id'] = !is_null($request->tax_id) ? $request->tax_id : env('DEFAULT_TAX_ID', 0);

        $product = Product::find($product_data['id']);

        $product->fill($product_data);

        $product->save();

        // Validate product images

        // if (count($product_images) > 0) {

        //     foreach ($product_images as $file) {
        //         $folderPath = "images/" . $store->id . "/";
        //         $image_parts = explode(";base64,", $file);

        //         // Validate if is base64 encoded
        //         if (count($image_parts) > 1) {

        //             $image_type_aux = explode("image/", $image_parts[0]);
        //             $image_type = $image_type_aux[1];
        //             $image_base64 = base64_decode($image_parts[1]);
        //             $name = $folderPath . uniqid() . '.' . $image_type;

        //             if (Storage::disk('public_uploads')->put($name, $image_base64)) {

        //                 DB::table('product_images')->insert(['product_id' => $product->id, 'image' => $name]);

        //                 if ($product_data['main_image']  == '*') {
        //                     $product_data['main_image']  = $name;
        //                     $product->main_image = $name;
        //                     $product->save();
        //                 }
        //             } else {
        //                 $response = [
        //                     'error' => 'File upload failed',
        //                     'message' => 'We could not upload your file'
        //                 ];
        //                 return response()->json($response, 401);
        //             }
        //         }
        //     }
        // }

        $product_categories = $request->input('product_categories.*');

        DB::table('product_category')->where('product_id', $product->id)->delete();

        foreach ($product_categories as $category) {
            DB::table('product_category')->insert(['product_id' => $product->id, 'category_id' => $category]);
        }

        // $product_variations = $request->input('product_variation.*');

        // DB::table('product_variation')->where('product_id', $product->id)->delete();

        // if ($product_variations) {
        //     foreach ($product_variations as $variation) {
        //         DB::table('product_variation')->insert(['product_id' => $product->id, 'variation' => $variation]);
        //     }
        // }

        // if ($request->get('reset_stock') && $request->get('reset_stock') == true) {
        //     DB::table('order_lines')->where('product_id', $product->id)->update(['processed' => true]);
        // }

        return response()->json($product, 200);
    }

    public function view($store, $id)
    {
        $store_id = Store::where('store_subdomain', $store)->first()->id;
        $product = Product::where('store_id', $store_id)->where('id', $id)->first();
        // $product->main_image = env('APP_URL') . '/public/' . $product->main_image;
        return $product;
    }

    public function delete(Request $request)
    {
        $request->validate([
            'ids' => 'required',
        ]);

        $ids = $request->input('ids.*');

        $categories = DB::table('product_category')->where(['product_id' => $request->id])->delete();

        $deleted = Product::whereIn('id', $ids)->where('user_id', auth()->user()->id)->delete();

        if (!$deleted) {
            $response = [
                'error' => 'Not found',
                'message' => 'The product was not found',
                'type' => 'info'
            ];

            return response()->json($response, 404);
        }

        $response = [
            'success' => 'Action performed',
            'message' => 'Row count: ' . $deleted
        ];

        return response()->json($response, 200);
    }

    public function removePicture(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric',
            'product_id' => 'required|numeric',
            'image' => 'required|string'
        ]);

        $image_data = $request->all();

        Storage::disk('public_uploads')->delete($image_data['image']);

        $response = DB::table('product_images')->where(['id' => $image_data['id'], 'product_id' => $image_data['product_id']])->delete();

        return response()->json($response, 200);
    }

    public function addToFavorite(Request $request)
    {
        $request->validate([
            'product_id' => 'required|numeric',
        ]);

        $product = Product::find($request->product_id);

        if (is_null($product)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        $exists = DB::table('favorites')->where('product_id', $product->id)->where('user_id', auth()->user()->id)->get()->first();

        if (is_null($exists)) {
            DB::table('favorites')->insert(['product_id' => $product->id, 'user_id' => auth()->user()->id]);
        } else {
            $response = [
                'error' => 'Already exists',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        return response()->json($product, 200);
    }

    public function listFavorites($limit = 0)
    {
        $pivot = 0;
        $favorites = DB::table('favorites')->where('user_id', auth()->user()->id)->get()->shuffle();

        $products = array();

        foreach ($favorites as $favorite) {
            $product = Product::where('id', $favorite->product_id)->where('availability', true)->where('archived', false)->get()->first();
            if (!is_null($product) && $product->store->is_active == true) {
                array_push($products, $product);
                $pivot++;
            }

            if ($limit > 0 && $limit == $pivot) break;
        }

        return response()->json($products, 200);
    }

    public function listSimilars($id, $limit = 0)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            $response = [
                'error' => 'Not authorized',
                'message' => 'This action has been logged and you may be suspended'
            ];
            return response()->json($response, 401);
        }

        // $products = Array();

        $categories = DB::table('product_category')->where('product_id', $id)->get();

        $products_ids = DB::table('product_category')->whereIn('category_id', $categories->pluck('category_id'))->get()->pluck('product_id')->unique();

        if ($limit > 0) {
            $products = Product::whereIn('id', $products_ids)->where('availability', true)->where('archived', false)->take($limit)->get();
        } else {
            $products = Product::whereIn('id', $products_ids)->where('availability', true)->where('archived', false)->get();
        }

        $arr_products = array();

        foreach ($products as $key => $product) {
            if (!$product->store->is_active) {
                $products->forget($key);
            } else {
                array_push($arr_products, $product);
            }
        }

        return response()->json($arr_products, 200);
    }

    public function listPublicStarred($limit = 0)
    {
        return $this->listStarred(null, $limit);
    }

    public function listStarred($store, $limit = 0)
    {
        $pivot = 0;

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $starreds = Product::where('availability', true)->where('archived', false)
                ->where('starred', true)
                ->where('store_id', $store->id)->get()->shuffle();
        } else {
            $starreds = Product::where('starred', true)->where('availability', true)->where('archived', false)->get()->shuffle();
        }

        $products = array();

        foreach ($starreds as $starred) {

            if ($starred->store->is_active == true) {
                unset($starred->questions_and_answers);
                unset($starred->categories);

                if ($starred->store->is_active == true) {
                    array_push($products, $starred);
                    $pivot++;
                }

                if ($limit > 0 && $limit == $pivot) break;
            }
        }

        // dd($products);

        return response()->json($products, 200);
    }

    public function listStoreStarred($store, $limit = 0)
    {
        return $this->listStarred($store, $limit);
    }

    public function listPublicDefaultCategory($limit = 0)
    {
        return $this->listDefaultCategory(null, $limit);
    }

    public function listDefaultCategory($store, $limit = 0)
    {
        $pivot = 0;

        $defaultCategory = Category::where('default', true)->get()->first();

        $arr_products = array();

        if (!is_null($defaultCategory)) {
            $arr_products = DB::table('product_category')->where('category_id', $defaultCategory->id)->get()->pluck('product_id')->unique();
        }

        if ($store) {
            $store = Store::where('store_subdomain', $store)->get()->first();
            $chosen = Product::where('availability', true)->where('archived', false)
                ->whereIn('id', $arr_products)
                ->where('store_id', $store->id)->get()->shuffle();
        } else {
            $chosen = Product::whereIn('id', $arr_products)->where('availability', true)->where('archived', false)->get()->shuffle();
        }

        $products = array();

        foreach ($chosen as $product) {
            $product->defaultCategoryName = $defaultCategory->category_name;

            if ($product->store->is_active == true) {
                array_push($products, $product);
                $pivot++;
            }

            if ($limit > 0 && $limit == $pivot) break;
        }

        return response()->json($products, 200);
    }

    public function toggleArchive(Request $request)
    {
        $request->validate([
            'ids' => 'required',
        ]);

        $ids = $request->input('ids.*');

        $archived = Product::whereIn('id', $ids)->where('user_id', auth()->user()->id)->get();

        foreach($archived as $product) {
            $product->archived = !$product->archived;
            $product->save();
        }

        if (!$archived) {
            $response = [
                'error' => 'Not found',
                'message' => 'The product was not found',
                'type' => 'info'
            ];

            return response()->json($response, 404);
        }

        $response = [
            'success' => 'Action performed',
            'message' => 'Row count: ' . $archived->count()
        ];

        return response()->json($response, 200);
    }

    public function editListPrice(Request $request)
    {     
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $products = $request->input('products');

        $allPricesArePositive = collect($products)->every(function ($product) {
            return $product['product_price'] >= 0;
        });

        if (!$allPricesArePositive) {
            return response()->json(['message' => 'All prices must be positive values'], 400);
        }

        foreach ($products as $product) {
            $product_id = $product['product_id'];
            $product_price = $product['product_price'];
            Product::where('id', $product_id)->where('user_id', auth()->user()->id)->update(['price' => $product_price]);
        }

        return response()->json(['message' => 'Prices updated correctly'], 200);
    }
}
