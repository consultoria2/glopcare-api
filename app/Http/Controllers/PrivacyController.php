<?php

namespace App\Http\Controllers;

use App\Models\Privacy;
use App\Models\User;
use Illuminate\Http\Request;

class PrivacyController extends Controller
{
    public function updatePrivacy(Request $request)
    {

        $privacy = Privacy::updateOrCreate(
            ['id' => 1],
            [
                'privacies_content' => $request->privacies_content,
            ]
        );

        return $privacy;
    }
}
