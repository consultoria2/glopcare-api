<?php

namespace App\Http\Controllers;

use App\Models\DataDeletion;
use Illuminate\Http\Request;

class DataDeletionController extends Controller
{
    public function updateDataDeletion(Request $request)
    {
        $dataDeletion = DataDeletion::updateOrCreate(
            ['id' => $request->id],
            [
                'data_deletions_content' => $request->data_deletions_content,
            ]
        );

        return $dataDeletion;
    }
}
