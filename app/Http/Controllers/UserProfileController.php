<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    //
    public function setProfile(Request $request){
        
        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        if (isset($request['close_session']) && ($request['close_session'] == true || $request['close_session'] == 1 || $request['close_session'] == 'true')) {
            $user->last_device_id = null;
            $user->last_device_info = null;
            $user->save();
        }

        $profile = UserProfile::where('user_id', $user->id)->where('is_active', true)->get()->first();

        if (is_null($profile)) {
            
            $new_profile = new UserProfile();

            $new_profile->user_id = $user->id;
            $new_profile->profile = $element['profile'];

            $new_profile->save();

            return response()->json([
                'message' => 'Profile created',
                'type' => 'info',
                'profile' => $new_profile
            ], 200);
            
        } else {
            $profile->is_active = $element['is_active'];
            $profile->profile = $element['profile'];
            $profile->save();

            return response()->json([
                'message' => 'Profile updated',
                'type' => 'info',
                'profile' => $profile
            ], 200);
        }
    }

    public function getProfile(Request $request){
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $profile = UserProfile::where('user_id', $user->id)->where('is_active', true)->get()->first();


        if (is_null($profile)) {
            return response()->json([
              'message' => 'Profile not found'
            ], 401);
        } else {
            return $profile;
        }
    }
}
