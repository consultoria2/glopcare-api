<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\CustomerOrderDetail;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;

use App\Models\Store;
use App\Models\StoreSetting;
use App\Models\Product;
use App\Models\Tax;
use App\Models\User;

use App\Models\Order;
use App\Models\OrderLines;
use App\Notifications\NewOrder;
use App\Mail\OrderDetail;
use App\Models\OrderPaymentMethods;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use stdClass;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $data = User::orderBy('id', 'DESC')->paginate(5);
        // return view('users.index', compact('data'))
        //     ->with('i', ($request->input('page', 1) - 1) * 5);

        $orders = Order::where('store_id', auth()->user()->id)->orderBy('id', 'ASC')->get();
        //dd($orders);
        return view('orders', compact('orders'));
    }

    public function setStatus($status, $orderId)
    {
        if (env('GLOPCARE_USER_ID') == auth()->user()->id) {
            $order = Order::find($orderId);
        } else {
            $order = Order::where('id', $orderId)->where('store_id', auth()->user()->id)->get()->first();
        }

        if (!is_null($order && in_array($status, ['inprogress', 'delivered', 'cancelled']))) {
            $order->status = $status;
            $order->save();
        }

        return view('order', ['order' => $order]);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'customer_name' => 'required',
            'customer_email' => 'required',
            'customer_phone' => 'required',
            'products' => 'required',
            'shipping_address' => 'required'
        ]);

        $input = $request->all();

        //---------------------------------------------------------------
        //-- Receiving Payment Methods
        //---------------------------------------------------------------        

        $aux_payment_methods = $request->input('payment_methods.*');

        $payment_methods = array();
        foreach ($aux_payment_methods as $payment_method) {
            array_push($payment_methods, $payment_method);
        }
        //---------------------------------------------------------------
        //-- Separating lines by store
        //---------------------------------------------------------------        

        $orders = array();

        $aux_products = $request->input('products.*');

        $products = array();

        foreach ($aux_products as $product) {
            array_push($products, $product);
        }

        foreach ($products as $line) {
            if (!array_key_exists($line['user_id'], $orders)) {
                $orders[$line['user_id']] = array();
            }
            array_push($orders[$line['user_id']], $line);
        }

        //---------------------------------------------------------------
        //-- Preparing transaction
        //---------------------------------------------------------------

        $saved_orders = array();

        DB::beginTransaction();

        foreach ($orders as $key => $lines) {

            //---------------------------------------------------------------
            //-- Retrieving user
            //---------------------------------------------------------------
            $store = User::find($key);
            $order = array();

            if (is_null($store)) {
                $response = [
                    'error' => 'Not found',
                    'code' => 404,
                    'message' => 'User ' . $key . ' not found'
                ];
                throw new \Exception($response);
            }

            $order['user_id'] = $store->id;

            //---------------------------------------------------------------
            //-- Retrieving store order number
            //---------------------------------------------------------------

            $setting = StoreSetting::where('setting_name', 'next_order_number')->where('store_id', $store->id)->get();
            $next_order_number = 1;

            if ($setting->count()) {
                $setting = $setting->first();
                $next_order_number = $setting->setting_data;
                $next_order_number++;
                $setting->setting_data = $next_order_number;
                $setting->save();
            } else {
                $setting = [];
                $setting['store_id'] = $store->id;
                $setting['setting_name'] = 'next_order_number';
                $setting['setting_data'] = $next_order_number;
                StoreSetting::create($setting);
            }

            $order['store_order_number'] = $next_order_number;

            //---------------------------------------------------------------
            //-- Setting order customer data
            //---------------------------------------------------------------

            $order['customer_name']  = $input['customer_name'];
            $order['customer_email'] = $input['customer_email'];
            $order['customer_phone'] = $input['customer_phone'];


            //---------------------------------------------------------------
            //-- Setting order related data
            //---------------------------------------------------------------

            $order['shipping_address'] = $input['shipping_address'];
            $order['currency'] = 'USD';
            $order['comments'] = '';
            $order['store_id'] = $store->id;
            $order['net_amount'] = 0;
            $order['tax_amount'] = 0;
            $order['total_amount'] = 0;
            $order['total_lines'] = 0;

            if (isset($input['coupon_id'])) {
                $order['coupon_id'] = $input['coupon_id'];
            }

            $order['total_discount'] = 0;

            //---------------------------------------------------------------
            //-- Saving order's header
            //---------------------------------------------------------------

            $order = Order::create($order);
            $order->net_amount = 0;
            $order->tax_amount = 0;
            $order->total_amount = 0;
            $order->total_lines = 0;
            $order->total_discount = 0;

            //---------------------------------------------------------------
            //-- Retrieving coupon value
            //---------------------------------------------------------------

            $coupon = null;

            if (isset($input['coupon_id'])) {
                $coupon = Coupon::where('is_active', true)->where('id', $input['coupon_id'])->get()->first();

                if (is_null($coupon)) {
                    $response = [
                        'error' => 'Not found',
                        'code' => 404,
                        'message' => 'Coupon not found'
                    ];
                    DB::rollBack();
                    return response()->json($response, $response['code']);
                }
            }


            //---------------------------------------------------------------
            //-- Saving order's lines
            //---------------------------------------------------------------

            foreach ($lines as $line) {
                $product = Product::where('id', $line['id'])->where('user_id', $store->id)->get()->first();

                if ($product) {
                    $order_line['order_id'] = $order->id;
                    $order_line['product_id'] = $line['id'];
                    $order_line['qty'] = intval($line['qty']);

                    if ($line['discount_price'] && floatval($line['discount_price']) < floatval($line['price'])) {
                        $order_line['net_price'] = floatval($line['discount_price']);
                    } else {
                        $order_line['net_price'] = floatval($line['price']);
                    }


                    $order_line['net_amount'] = $order_line['qty'] * $order_line['net_price'];

                    if (!is_null($coupon)) {
                        $order_line['discount_amount'] = $order_line['net_amount'] * $coupon->coupon_value;
                    } else {
                        $order_line['discount_amount'] = 0;
                    }

                    if (isset($line->selected_variation)) {
                        $order_line['comments'] = $line->selected_variation;
                    }

                    $tax = $product->tax;

                    if ($tax) {
                        $order_line['tax_amount'] = ($order_line['net_amount'] - $order_line['discount_amount']) * $tax->tax_value;
                    } else {
                        $order_line['tax_amount'] = ($order_line['net_amount'] - $order_line['discount_amount']) * 0;
                        // $response = [
                        //     'error' => 'Not found',
                        //     'code' => 404,
                        //     'message' => 'Tax for product ' . $product->product_name . ' not found'
                        // ];
                        // DB::rollBack();
                        // return response()->json($response, $response['code']);
                    }
                    OrderLines::create($order_line);

                    $order->net_amount += $order_line['net_amount'];
                    $order->tax_amount += $order_line['tax_amount'];
                    $order->total_discount += $order_line['discount_amount'];
                    $order->total_amount += ($order_line['net_amount'] - $order_line['discount_amount'] + $order_line['tax_amount']);
                    $order->total_lines++;
                } else {
                    $response = [
                        'error' => 'Not found',
                        'code' => 404,
                        'message' => 'Product (' . $line['id'] . ' - ' . $line->product_name . ') not found in the store ' . $store->store_name
                    ];
                    DB::rollBack();
                    return response()->json($response, $response['code']);
                }
            }

            //---------------------------------------------------------------
            //-- Saving Payment Methods
            //---------------------------------------------------------------

            if (!empty($payment_methods)) {
                foreach ($payment_methods as $order_payment_method) {
                    $orderPaymentMethod = new OrderPaymentMethods;
                    $orderPaymentMethod->order_id = $order->id;
                    $orderPaymentMethod->id_payment_method = $order_payment_method['id_payment_method'];
                    $orderPaymentMethod->amount = $order_payment_method['amount'];
                    $orderPaymentMethod->confirmation_number = $order_payment_method['confirmation_number'];
                    $orderPaymentMethod->save();
                }
            }

            $order->status = 'received';
            $order->save();

            array_push($saved_orders, $order);
        }

        DB::commit();

        foreach ($saved_orders as $key => $order) {
            $user = User::find($order->store_id);

            // Notify the Glopcard owner
            try {
                $user->notify(new NewOrder($order, $user));
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }

            // Notify the customer
            try {
                $customer = new stdClass;
                $customer->name = $order->customer_name;
                Mail::to($order->customer_email)->send(new CustomerOrderDetail($customer, $order, $payment_methods));
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }

            // Notify glopcare
            try {
                $glopcare = User::where('email', 'atencionalcliente@glopcare.com')->get()->first();
                if (!is_null($glopcare)) {
                    $glopcare->notify(new NewOrder($order, $user));
                }
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }

        return response()->json($saved_orders, 200);
    }
}
