<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentMethodController extends Controller
{

    public function createMethod(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        };
        $elements = json_decode(json_encode($request->all()));
        foreach ($elements as $element) {
            $paymentMethod = PaymentMethod::updateOrCreate([
                'user_id' =>  $user->id,
                'id' => $element->id,
            ], [
                'currency' => $element->currency ? $element->currency : 'USD',
                'name' => $element->name,
                'instructions' => $element->instructions,
                'is_active' => true,
                'slug' => 'slug',
                'updated_at' => Carbon::now(),
            ]);

            $element->id = $paymentMethod->id;
        }
        return $elements;
    }

    public function deleteMethod(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        };

        DB::table('payment_methods')
            ->where('user_id', $user->id)
            ->where('id', $request->id)
            ->delete();

        return response()->json([
            'message' => 'Payment method deleted successfully'
        ], 200);
    }

    public function listMethods()
    {$user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        };
        $paymentMethods = PaymentMethod::where('user_id', $user->id)
            ->orderByDesc('id')
            ->get();
        return $paymentMethods;
    }

    public function listPaymentMethods2()
    {$user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        };
        
    }

    public static function listPaymentMethods($user_id = null)
    {
        $user = User::find($user_id ? $user_id : auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
                'message' => 'User not found'
            ], 401);
        }

        $paymentMethods = PaymentMethod::where('user_id', $user->id)
        ->orderByDesc('id')
        ->get();
        
    return $paymentMethods;
    }
}
