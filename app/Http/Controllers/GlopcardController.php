<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Glopcard;
use App\Models\Product;
use App\Models\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
class GlopcardController extends Controller
{
    //
    public function setGlopcard(Request $request){
        
        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $glopcard = Glopcard::where('user_id', $user->id)->where('is_active', true)->get()->first();

        if (is_null($glopcard)) {
            
            $new_glopcard = new Glopcard();

            $new_glopcard->user_id = $user->id;
            $new_glopcard->snapshot = $element['snapshot'];
            $new_glopcard->template_id = $element['template_id'];

            $new_glopcard->save();

            $glopcard = Glopcard::where('user_id', $user->id)->where('is_active', true)->get()->first();

            return response()->json([
                'message' => 'Glopcard created',
                'type' => 'info',
                'snapshot' => $glopcard
            ], 200);
            
        } else {
            $glopcard->is_active = true;
            $glopcard->snapshot = $element['snapshot'];
            $glopcard->save();

            return response()->json([
                'message' => 'Glopcard updated',
                'type' => 'info',
                'snapshot' => $glopcard
            ], 200);
        }
    }

    public function getPrivateGlopcard(Request $request){
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $glopcard = Glopcard::where('user_id', $user->id)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Glopcard not found',
              'type' => 'info',
            ], 401);
        } else {
            return $glopcard;
        }
    }

    public function getPublicGlopcard($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Glopcard not found',
            ], 401);
        } else {
            $user = User::find($glopcard->user_id);

            if (is_null($user)) {
                return response()->json([
                  'message' => "Glopcard's user not found",
                ], 401);
            }

            $glopcard->views = $glopcard->views + 1;
            $glopcard->save();

            $snapshot =  json_decode($glopcard->snapshot);
            $snapshot->username = $user->username;
            
            return $snapshot;
        }
    }

    public function setCatalog(Request $request){
        
        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $catalog = json_decode($element['catalog']);

        // Delete catalog entries if IDs does not exist
        $ids = Arr::pluck($catalog, 'id');
        if (count($ids) == 0) {
            DB::table('glopcard_catalog_group')->where('user_id', $user->id)->whereNotIn('id', Arr::pluck($catalog, 'id'))->delete();
        } else {
            DB::table('glopcard_catalog_group')->where('user_id', $user->id)->whereNotIn('id', Arr::pluck($catalog, 'id'))->delete();
        }
        

        foreach($catalog as $group){

            //Upsert group names

            if(property_exists($group, 'id')){
                $catalogId = $group->id;
                DB::table('glopcard_catalog_group')->where('id', $group->id)->update(['group_name' => $group->group_name, 'updated_at' => Carbon::now()]);
            } else {
                $catalogId = DB::table('glopcard_catalog_group')->insertGetId(['group_name' => $group->group_name, 'user_id' => $user->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }

            // Delete catalog images if IDs does not exist
            DB::table('glopcard_catalog_group_image')->where('catalog_id', $catalogId)->whereNotIn('id', Arr::pluck($group->group_layout, 'id'))->delete();

            foreach($group->group_layout as $image){

                //Upsert group images

                if (isset($image->image_binary)){
                    $image = $image->image_binary;
                    
                    $extension = 'png';

                    if (strpos($image, 'image/jpeg')){
                        $extension = 'jpg';
                        $image = str_replace('data:image/jpeg;base64,', '', $image);
                    } else {
                        $image = str_replace('data:image/png;base64,', '', $image);
                    }
                    
                    $image = str_replace(' ', '+', $image);
                    $safeName = Str::uuid() . '.' . $extension;
                    $success = file_put_contents(public_path() . '/storage/uploads/' . $safeName, base64_decode($image));

                    // Generating webp version only on png
                    if ($extension == 'png'){
                        $webp = Image::make(public_path() . '/storage/uploads/' . $safeName);
                        $webp->encode('webp', 80)->save(public_path() . '/storage/uploads/' . pathinfo($safeName, PATHINFO_FILENAME) . '.webp');
                        $safeName = pathinfo($safeName, PATHINFO_FILENAME) . '.webp';
                    }

                } else {
                    // Prevent uploading and deleting image
                    $success = true;
                    $safeName = $image->image_uri;
                }

                if ($success){

                    $position = 5;

                    if (property_exists($image, 'position')){
                        if ($image->position == 0) {
                            $position = 5;
                        } else {
                            $position = $image->position;
                        }
                    }

                    if(property_exists($image, 'id')){
                        DB::table('glopcard_catalog_group_image')->where('id', $image->id)->update(['image_uri' => $safeName, 'updated_at' => Carbon::now()]);
                    } else {
                        DB::table('glopcard_catalog_group_image')->insert(['image_uri' => $safeName, 'catalog_id' => $catalogId, 'position' => $position, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                    }
                } else {
                    return response()->json([
                        'message' => 'Could not create file image',
                        'type' => 'info',
                    ], 503);
                }
                
            }
        }

        return response()->json([
            'message' => 'Glopcard catalog updated',
            'type' => 'info',
        ], 200);
    }

    public function getPrivateCatalog(Request $request){
        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $arr_catalog = Array();

        $catalogs = DB::table('glopcard_catalog_group')->where('user_id', $user->id)->where('is_active', true)->get();

        foreach($catalogs as $catalog){
            $toMapDomain = DB::table('glopcard_catalog_group_image')->where('catalog_id', $catalog->id)->where('is_active', true)->orderBy('position')->get()->toArray();

            $arr_uri_domain = Array();

            // Prepare for the frontend

            foreach($toMapDomain as $key => $image){
                $image->url = 'https://glopcare.com/storage/uploads/' . $image->image_uri;

                $nextY = intval($key / 2);
                $nextX = $key % 2;
                $nextI = $key;

                $image->x = $nextX;
                $image->y = $nextY;
                $image->w = 1;
                $image->h = 1;
                $image->i = $nextI;

                $style = new \stdClass;
                $style->backgroundSize = 'contain';
                $style->backgroundRepeat = 'no-repeat';
                $style->backgroundImage = 'url(' . $image->url . ')';

                $image->style = $style;

                array_push($arr_uri_domain, $image);
            }

            $catalog->group_layout = $arr_uri_domain;

            array_push($arr_catalog, $catalog);
        }

        return $arr_catalog;
    }

    public function getPublicCatalog($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        $arr_catalog = Array();

        $catalogs = DB::table('glopcard_catalog_group')->where('user_id', $glopcard->user_id)->where('is_active', true)->get();

        foreach($catalogs as $catalog){
            $toMapDomain = DB::table('glopcard_catalog_group_image')->where('catalog_id', $catalog->id)->where('is_active', true)->orderBy('position')->get()->toArray();

            $arr_uri_domain = Array();

            // Prepare for the frontend

            foreach($toMapDomain as $key => $image){
                $image->url = 'https://glopcare.com/storage/uploads/' . $image->image_uri;

                $nextY = intval($key / 2);
                $nextX = $key % 2;
                $nextI = $key;

                $image->x = $nextX;
                $image->y = $nextY;
                $image->w = 1;
                $image->h = 1;
                $image->i = $nextI;

                $style = new \stdClass;
                $style->backgroundSize = 'contain';
                $style->backgroundRepeat = 'no-repeat';
                $style->backgroundImage = 'url(' . $image->url . ')';

                $image->style = $style;

                array_push($arr_uri_domain, $image);
            }

            $catalog->group_layout = $arr_uri_domain;

            array_push($arr_catalog, $catalog);
        }

        return $arr_catalog;
    }

    public function listTemplates(){

        //Obtaining all templates
        $templates = Template::where('is_active', true)->get();
        
        foreach ($templates as $key => $value) {
            // Obtaning templates that have an specific assignation
            $filteredTemplate = DB::table('glopcard_user_template')->where('template_id', $value->id)->where('is_active', true)->get();

            // If the template has a specific assignation, check if it exists for the given user, if not, then remove it
            if ($filteredTemplate->count() > 0){

                if (!in_array(auth()->user()->id, $filteredTemplate->pluck('user_id')->toArray())) {
                    $templates->forget($key);
                }
            }
        }

        return $templates;
    }

    public function sortCatalogGallery(Request $request){
        $element = $request->all();

        $user = User::find(auth()->user()->id);

        if (is_null($user)) {
            return response()->json([
              'message' => 'User not found'
            ], 401);
        }

        $gallery = json_decode($element['gallery']);

        foreach($gallery as $image){

            //Upsert group names

            if(property_exists($image, 'id')){
                $imageId = $image->id;
            } else {
                return response()->json([
                    'message' => 'Can not sort because image id is not present',
                    'type' => 'info',
                ], 503);
            }

            $position = 0;

            if ($image->x == 0 && $image->y == 0){
                $position = 1;
            }

            if ($image->x == 1 && $image->y == 0){
                $position = 2;
            }

            if ($image->x == 0 && $image->y == 1){
                $position = 3;
            }

            if ($image->x == 1 && $image->y == 1){
                $position = 4;
            }

            DB::table('glopcard_catalog_group_image')->where('id', $imageId)->update(['position' => $position, 'updated_at' => Carbon::now()]);

        }

        return response()->json([
            'message' => 'Glopcard catalog gallery updated',
            'type' => 'info',
        ], 200);
    }

    public function getPublicCatalogItems($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        return ProductController::listAll($glopcard->user_id);
    }

    public function getPublicCatalogProduct($uid, $product_id){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        $product = Product::where('user_id', $glopcard->user_id)->where('id', $product_id)->where('archived', false)->get()->first();

        if (is_null($product)) {
            return response()->json([
              'message' => 'Product not found'
            ], 401);
        }


        return $product;
    }

    public function getPublicCategories($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        return ProductController::listCategories($glopcard->user_id);
    }

    public function getPublicGroups($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        return ProductController::listGroups($glopcard->user_id);
    }

    
    public function getPublicCatalogProductSimilars($uid, $product_id, $limit = 4)
    {
        $pivot = 0;

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        $product = Product::where('user_id', $glopcard->user_id)->where('id', $product_id)->where('archived', false)->get()->first();

        if (is_null($product)) {
            return response()->json([
              'message' => 'Product not found'
            ], 401);
        }

        $defaultCategories = $product->categories;

        $arr_products = array();

        if ($defaultCategories && count($defaultCategories) > 0) {
            $arr_products = DB::table('product_category')->whereIn('category_id', $defaultCategories)->get()->pluck('product_id')->unique();
        }

        if (!is_array($arr_products)){
            $arr_products = $arr_products->toArray();
        }

        $chosen = Product::where('availability', true)->where('archived', false)
            ->whereIn('id', array_diff($arr_products, array($product_id)))
            ->where('user_id', $glopcard->user_id)->get()->shuffle();

        $complement = array();

        if ($chosen->count() < 5) {
            $complement = Product::where('availability', true)->where('archived', false)
            ->whereNotIn('id', $arr_products)
            ->where('user_id', $glopcard->user_id)->get()->shuffle();
        }


        $products = array();

        foreach ($chosen as $product) {
            array_push($products, $product);
            $pivot++;
            
            if ($limit > 0 && $limit == $pivot) break;
        }

        // foreach ($complement as $product) {
        //     array_push($products, $product);
        //     $pivot++;
            
        //     if ($limit > 0 && $limit == $pivot) break;
        // }

        return response()->json($products, 200);
    }

    
    public function getPublicPaymentMethods($uid){

        $glopcard = Glopcard::where('uid', $uid)->where('is_active', true)->get()->first();


        if (is_null($glopcard)) {
            return response()->json([
              'message' => 'Catalog not found'
            ], 401);
        }

        return PaymentMethodController::listPaymentMethods($glopcard->user_id);
    }
}
