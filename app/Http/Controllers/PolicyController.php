<?php

namespace App\Http\Controllers;

use App\Models\Policy;
use App\Models\User;
use Illuminate\Http\Request;

class PolicyController extends Controller
{
    public function updatePolicy(Request $request)
    {


        $policy = Policy::updateOrCreate(
            ['id' => 1],
            [
                'policies_content' => $request->policies_content,
            ]
        );

        return $policy;
    }
}
