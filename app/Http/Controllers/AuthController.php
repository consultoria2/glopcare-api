<?php

namespace App\Http\Controllers;

use App\Mail\DeleteAccountMessageMail;
use App\Mail\DeleteMail;
use App\Models\DeleteAccountMessage;
use App\Models\DeletionCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Notifications\VerifyUser;
use App\Notifications\NewPasswordCreated;
use App\Notifications\AccountDeleted;
use App\Notifications\VerifiedUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    $validatedData = $request->validate([
      'email' => 'required|string|email|max:255|unique:users',
      'username' => 'required|string|max:255|unique:users',
      'password' => 'required|string|min:8',
    ]);

    if (!isset($validatedData['name'])) {
      $validatedData['name'] = explode('@', $validatedData['email'])[0];
    }

    $user = User::create([
      'name' => $validatedData['name'],
      'email' => $validatedData['email'],
      'username' => $validatedData['username'],
      'password' => Hash::make($validatedData['password']),
      'verification_token' => Str::random(20)
    ]);

    $token = $user->createToken('auth_token')->plainTextToken;

    $user->notify(new VerifyUser($user));

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function completeRegister(Request $request)
  {
    $validatedData = $request->validate([
      'username' => 'required|string|max:255|unique:users',
    ]);

    $user = User::find(auth()->user()->id);
    $user->username = $validatedData['username'];
    $user->request_username = false;
    $user->status = 'verified';

    $user->save();

    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function phoneRegister(Request $request)
  {
    $validatedData = $request->validate([
      'phone' => 'required|string|max:255|unique:users',
      'username' => 'required|string|max:255|unique:users',
      'password' => 'required|string|min:8',
    ]);

    if (!isset($validatedData['name'])) {
      $validatedData['name'] = trim($validatedData['phone']);
    }

    $user = User::create([
      'name' => $validatedData['name'],
      'phone' => $validatedData['phone'],
      'email' => $validatedData['phone'],
      'username' => $validatedData['username'],
      'password' => Hash::make($validatedData['password']),
      'verification_token' => Str::random(20)
    ]);

    $token = $user->createToken('auth_token')->plainTextToken;

    $user->notify(new VerifyUser($user));

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function login(Request $request)
  {

    $previousSocialLogin = DB::table('social_login')->where('email', $request->get('email'))->get()->first();

    if (!is_null($previousSocialLogin)) {
      return response()->json([
        'message' => 'Should login using social network',
        'social' => $previousSocialLogin->social_network_code
      ], 409);
    }

    $user = User::where('email', $request['email'])->firstOrFail();

    if (!Auth::attempt($request->only('email', 'password'))) {
      return response()->json([
        'message' => 'Invalid login details'
      ], 401);
    }

    if ($user->status == 'created') {
      return response()->json([
        'message' => 'Verify your account',
      ], 410);
    }

    $token = $user->createToken('auth_token')->plainTextToken;

    // Validate last session device ID

    $warning_message = null;
    $last_device_info = null;

    if (!is_null($user->last_device_id) && $user->last_device_id != $request['device_id']) {
      $warning_message = 'No ha cerrado sesión en otro dispositivo. Cerraremos automáticamente la sesión en el dispositivo anterior.';
      $last_device_info = $user->last_device_info;
    }

    $user->last_device_id = isset($request['device_id']) ? $request['device_id'] : null;
    $user->last_device_info = isset($request['device_info']) ? $request['device_info'] : null;
    $user->save();
    
    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
      'warning_message' => $warning_message,
      'last_device_info' => $last_device_info
    ]);
  }

  public function phoneLogin(Request $request)
  {

    $user = User::where('phone', $request['phone'])->firstOrFail();

    if (!Auth::attempt($request->only('phone', 'password'))) {
      return response()->json([
        'message' => 'Invalid login details'
      ], 401);
    }

    if ($user->status == 'created') {
      return response()->json([
        'message' => 'Verify your account',
      ], 410);
    }

    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function googleLogin(Request $request)
  {
    $validatedData = $request->validate([
      'email' => 'required|string|email|max:255'
    ]);

    // Data may be lost after validation so we retrieve it again
    $responseData = $request->all();

    $user = User::where('email', $validatedData['email'])->get()->first();

    if (!isset($responseData['givenName'])) {
      $validatedData['name'] = '';
    } else {
      $validatedData['name'] = $responseData['givenName'];
    }

    // If the user does not exist, create from Google data response
    if (is_null($user)) {
      $user = User::create([
        'name' => $validatedData['name'],
        'email' => $validatedData['email'],
        'username' => $validatedData['email'],
        'password' => Hash::make(Str::random(8)),
        'verification_token' => Str::random(20),
        'request_username' => true
      ]);

      DB::table('social_login')->insert([
        'user_id' => $user->id,
        'email' => $validatedData['email'],
        'social_id' => $responseData['id'],
        'social_network_code' => 'GOO',
        'social_network_response' => json_encode($responseData),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }

    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function facebookLogin(Request $request)
  {
    $validatedData = $request->validate([
      'email' => 'required|string|email|max:255'
    ]);

    // Data may be lost after validation so we retrieve it again
    $responseData = $request->all();

    $user = User::where('email', $validatedData['email'])->get()->first();

    if (!isset($responseData['name'])) {
      $validatedData['name'] = explode('@', $validatedData['email'])[0];
    } else {
      $validatedData['name'] = $responseData['name'];
    }

    // If the user does not exist, create from Google data response
    if (is_null($user)) {
      $user = User::create([
        'name' => $validatedData['name'],
        'email' => $validatedData['email'],
        'username' => $validatedData['email'],
        'password' => Hash::make(Str::random(8)),
        'verification_token' => Str::random(20),
        'request_username' => true
      ]);

      DB::table('social_login')->insert([
        'user_id' => $user->id,
        'email' => $validatedData['email'],
        'social_id' => $responseData['id'],
        'social_network_code' => 'FB',
        'social_network_response' => json_encode($responseData),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);
    }

    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json([
      'access_token' => $token,
      'token_type' => 'Bearer',
    ]);
  }

  public function setPlayerId(Request $request)
  {
    $user = User::find(auth()->user()->id);

    if (!is_null($user)) {
      $user->player_id = $request->get('player_id');
      $user->save();
      return response()->json($user, 200);
    } else {
      return response()->json([
        'message' => 'User not found'
      ], 401);
    }
  }

  public function me(Request $request)
  {
    return $request->user();
  }

  public function restorePassword(Request $request)
  {
    $validatedData = $request->validate([
      'email' => 'required|string|max:255',
    ]);

    $user = User::where('email', $validatedData['email'])->orWhere('phone', $validatedData['email'])->get()->first();

    if (is_null($user)) {
      return response()->json([
        'message' => 'User not found'
      ], 401);
    }

    $status = Password::sendResetLink($request->only('email'));

    if ($status === Password::RESET_LINK_SENT) {
      return response()->json([
        'message' => 'Email was sent'
      ], 200);
    } else {
      return response()->json([
        'message' => 'Unknown error'
      ], 500);
    }
  }
  public function deleteAccount(Request $request)
  {
    $user = User::find(auth()->user()->id);

    $request->validate([
      'code' => 'required'
    ]);

    $verification_code = DeletionCode::where('user_id', $user->id)->where('code', $request->code)->first();   

    if (!$verification_code) {
      return response()->json(['error_code' => 'El código introducido es inválido']);
    }

    $expirationTime = Carbon::createFromFormat('Y-m-d H:i:s', $verification_code->expiration_time);

    if ($expirationTime->isPast()) {     
      return response()->json(['error_code' => 'El código ha expirado']);
    }
    
    $user = User::find(auth()->user()->id);

    $user->notify(new AccountDeleted($user));

    $deleted = DB::table('social_login')->where('email', $user->email)->orWhere('user_id', $user->id)->delete();

    $user->delete();
    $verification_code->delete();

    return response()->json([
      'message' => 'User deleted'
    ]);
  }
 
  public function sendDeleteMail(Request $request)
  {
    $user = User::find(auth()->user()->id);
    $code = strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 6));
    $expirationTime = Carbon::now()->addMinutes(5);

    DeletionCode::updateOrCreate(
      ['user_id' => $user->id],
      [
        'code' => $code,
        'expiration_time' => $expirationTime,
      ]
    );

    Mail::to(auth()->user()->email)->send(new DeleteMail($code));

    return response()->json([
      'message' => 'User mail send'
    ]);
  }
  public function deleteAccounMessage2(Request $request)
  {  

    return response()->json([
      'message' => $request->email
    ]);
  }
  public function deleteAccounMessage(Request $request)
  {   
   $message = DeleteAccountMessage::create([
    'message' => $request->message,
    'email' => $request->email,
    'name' => $request->name,
    ]);

    Mail::to("atencionalcliente@glopcare.com")->send(new DeleteAccountMessageMail($message));
    Mail::to("glopcare@glopcare.com")->send(new DeleteAccountMessageMail($message));

    return response()->json([
      'message' => $request->message
    ]);
  }
  public function verify($verification_token)
  {
    $user = User::where('verification_token', $verification_token)->whereIn('status', array('created', 'invited', 'verified'))->get()->first();

    if (is_null($user)) {
      return response()->json([
        'message' => 'User not found',
        'type' => 'info'
      ], 401);
    }

    $user->notify(new VerifiedUser($user));

    if (in_array($user->status, array('created', 'invited'))) {
      $user->status = 'verified';
      $user->save();

      return redirect()->route('verified-page', ['code' => '001']);
    } else {
      return redirect()->route('verified-page', ['code' => '002']);
    }
  }

  public function checkUsernameEmail(Request $request)
  {
    $validatedData = $request->validate([
      'username_email' => 'required|string|max:255'
    ]);

    $user = User::where('email', $validatedData['username_email'])->orWhere('username', $validatedData['username_email'])->get()->first();

    if (!is_null($user)) {
      return response()->json([
        'message' => 'Already in use',
        'type' => 'info'
      ], 409);
    }

    return response()->json([
      'message' => 'Free to use',
      'type' => 'info'
    ], 200);
  }

  public function checkDeviceId(Request $request)
  {
    return response()->json([
      'message' => 'Valid Device Id',
    ], 200);
  }
}
