<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            
            $table->string('product_type')->nullable();
            $table->string('product_name');
            $table->string('brand')->nullable();
            $table->decimal('discount_price', 12, 2)->nullable();
            $table->decimal('price',12, 2);
            $table->boolean('availability');
            $table->string('product_code');
            $table->string('price_type')->default('fixed');
            $table->text('tags')->nullable();
            $table->text('description')->nullable();
            $table->text('features')->nullable();
            $table->integer('group_id')->references('id')->on('product_group')->nullabe();
            $table->integer('stock');
            $table->integer('user_id')->references('id')->on('users');
            $table->boolean('archived')->default(false);

            $table->timestamps();

            $table->index(['user_id', 'product_code'])->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
