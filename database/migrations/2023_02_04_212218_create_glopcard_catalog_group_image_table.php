<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlopcardCatalogGroupImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glopcard_catalog_group_image', function (Blueprint $table) {
            $table->id();
            $table->integer('catalog_id')->references('id')->on('glopcard_catalog_group');
            $table->integer('views')->default(0);
            $table->boolean('is_active')->default(true);
            $table->binary('image_binary')->nullable();
            $table->string('image_uri')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glopcard_catalog_group_image');
    }
}
