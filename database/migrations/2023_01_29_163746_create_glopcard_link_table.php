<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateGlopcardLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glopcard', function (Blueprint $table) {
            $table->uuid('uid')->default(DB::raw('(UUID())'))->primary();
            $table->integer('user_id')->references('id')->on('users');
            $table->integer('views')->default(0);
            $table->json('snapshot')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glopcard_link');
    }
}
