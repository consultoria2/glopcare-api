# Privacy Policy

POLÍTICAS DE PRIVACIDAD 
Última actualización: 9 de febrero de 2022

Introducción

Para glopcare, tu privacidad es de vital importancia, por esta razón estamos comprometidos en protegerla con estas Políticas de Privacidad desarrolladas cuidadosamente, en cada uno de sus aspectos. Por favor, tómate un momento para leerlas y familiarizarte con ellas. Si tienes alguna duda, comunícate con nuestros asesores. Ten en cuenta, que al acceder a la plataforma glopcare, debes aceptar nuestras Condiciones de Uso, relacionadas a estas Políticas de Privacidad.
La información que obtenemos del uso de nuestros servicios, es de vital importancia para nosotros, porque con ella podemos recopilar datos sobre los servicios que usas, cómo los usas y también en qué momento revisas e interactúas con nuestros contenidos. De igual forma, nosotros (glopcare) podemos recopilar información específica de los diferentes dispositivos que utilizas, tales como: Modelo del hardware, versión del sistema operativo, identificadores de dispositivos únicos, información de la red móvil, entre otros, información que garantizamos que no compartiremos con terceras partes, que no estén involucradas en garantizarte un servicio óptimo y de calidad.
Esta aplicación puede descargarse desde el dominio www.glopcare.com y su descarga o utilización, atribuye la condición de usuario, a quien lo haga, e implica la aceptación de todas las condiciones incluidas en este documento de Políticas de Privacidad y en el Aviso Legal de dicha página Web.

1.	DEFINICIONES 

A continuación, se presentan los significados de los términos que encontrarás en este documento, los cuales consideramos que deben ser aclarados para evitar malas interpretaciones.

1.1. Términos y significados

Asesor glopcare: Representante que tendrás a tu disposición para canalizar tus dudas e inquietudes acerca de cualquier aspecto relacionado con la comercialización y el funcionamiento de la plataforma glopcare.

Asesor glopcare virtual: Representante virtual identificado con el nombre de Tom, que te asistirá en los trámites automatizados que podrás realizar, a través de nuestro WhatsApp Business.

Cliente: Es la persona que hace uso de la tarjeta de presentación inteligente glopcard, compartida por el usuario responsable de presentar los servicios y/o productos que ofreces en tu negocio o el negocio que representas, así como también, puede ser compartida por cualquier otra persona. 

Datos personales (o datos): Se refiere a cualquier información que directa, indirectamente o en conexión con otra información, incluido un número de identificación personal, permita la identificación de una persona física.

Información personal: Es aquella que te identifica a ti como usuario, puede ser usada para contactarte en línea o fuera de línea. Por ejemplo: tu nombre completo, dirección física, correo electrónico, teléfonos, redes sociales, fotografías o videos e información precisa de tu ubicación geográfica.

Infraestructura Backend: Es la parte de la plataforma que el usuario no puede ver, se accede con la información que se solicita, a través de la plataforma, para luego combinarla y devolverla al usuario.

Plataforma glopcare: Es el sistema que sirve como base, para hacer funcionar los módulos de hardware y software, a través de los cuales se recopila, se procesa y se presenta, la información ingresada por los usuarios.

Procesador de datos (o supervisor de datos): Es la persona física o jurídica, autoridad pública, agencia u otro organismo, que procesa los datos personales del usuario, en representación de glopcare.

Rastreador: El rastreador indica cualquier tecnología, por ejemplo, identificadores únicos, balizas web, scripts incrustados, etiquetas electrónicas y huellas digitales, que permiten el seguimiento de los usuarios, por ejemplo, al acceder o almacenar información en el dispositivo del usuario.
Esta declaración de privacidad ha sido preparada en base a disposiciones de múltiples legislaciones. Estas Políticas de Privacidad, se relaciona únicamente con esta plataforma, sino se indica lo contrario en este documento.

Responsable del tratamiento: Es la persona física o jurídica, autoridad pública, agencia u otro organismo que, solo o conjuntamente con otros, determine los fines y medios del tratamiento de datos personales, incluidas las medidas de seguridad relativas al funcionamiento y uso de esta plataforma. El controlador de datos, a menos que se especifique lo contrario, es un representante de glopcare.

Servicios glopcare: Es el servicio proporcionado por esta plataforma como se describe en las Condiciones de Uso, que brindamos a través de un link, una app o sitio web, que le permite al usuario crear una tarjeta de presentación inteligente y personalizada, en la cual podrá interactuar con sus clientes y presentar los servicios y/o productos, que ofrece como profesional o empresa dedicados al cuidado de la imagen personal

Sitio web: Es el sitio web ubicado en www.glopcare.com

Terceros: Son proveedores de servicios, proveedores de alojamiento, empresas de tecnología de información (TI), entre otros que, están involucradas en garantizar el funcionamiento de la plataforma glopcare.

URI: Es la abreviatura de Uniform Resource Identifier, en español Identificador Uniforme de Recursos. Este término genérico se emplea para todos los tipos de nombres y direcciones que se refieren a objetos de internet, tales como páginas, imágenes, videos, etc.

Usuario o usuario glopcare: Persona o representante de una organización o empresa, que descarga la plataforma y registra sus datos como persona responsable de su uso.

Visitante: Persona que accede a la información de la tarjeta de presentación glopcare, sin ser cliente del usuario de la misma. 

1.2. Interpretación

En estas Políticas de Privacidad

Las palabras o frases de otro color, con mayúsculas, en letras cursivas o resaltadas en negritas, son usadas para facilitar la lectura de este documento, y no afectan en ningún caso, la interpretación de estas Políticas de Privacidad.

Las palabras de cualquier género, siempre incluyen todos los géneros, sin ninguna excepción.

Una expresión que se refiere a una persona, incluye cualquier individuo o representante de una organización empresarial, de cualquier tipo.

Una referencia a cualquier legislación, reglamento, instrucciones vinculantes emitidas por agencias gubernamentales, códigos de conducta obligatorios o similares, incluye todos los instrumentos delegados hechos en virtud de ellos y cualquier enmienda, consolidación, reemplazo o nueva promulgación de cualquiera de ellos.

Especificar algo en estas Políticas de Privacidad, después de las palabras “incluir”, “por ejemplo”, “etc.”, “entre otros” o expresiones similares, no limita los elementos que pueden ser incluidos.

2. ACEPTACIÓN

Cuando ingresas a glopcare, es porque estás de acuerdo con todos estos términos vinculantes de aceptación

Aceptas estar sujeto a las Políticas de Privacidad de glopcare y además aceptas cumplirlas. Sino estás de acuerdo, con alguna parte de estas Políticas de Privacidad, no podrás acceder a los servicios glopcare.

Reconoces los términos específicos de la jurisdicción de los servicios de glopcare, desde la jurisdicción que corresponde, según el lugar donde tú estás ubicado, de tal forma que, al haber conflictos entre las disposiciones de estas Políticas de Privacidad, con respecto a la jurisdicción donde te encuentras, estos últimos prevalecerán en la medida de la inconsistencia.

3.	ADMINISTRADOR DE LOS DATOS

3.1.	Datos empresariales

El administrador de los datos proporcionados es: Nature Prism International LLC.

Propietario responsable de la empresa: Tibisay Pelayo

Ubicado en: 15715 S Dixie HWY 211. Miami, FL 33157

Número de identificación: L21000013913

Correo electrónico: glopcare@gmail.com

Para asuntos relacionados con tus datos personales, comunícate con el delegado de protección de datos, Maria Jordan, correo electrónico: glopcare@gmail.com 

3.2.	Información de los datos

Los datos que tú proporcionas para el uso de esta plataforma son procesados por glopcare. Estos datos, incluyen información que tú nos suministras, por ejemplo, al completar formularios o casillas de verificación en la plataforma.

3.3. Uso de los datos

Glopcare procesa los datos para dos propósitos: El propósito de cumplir con el acuerdo entre glopcare y el usuario; y el propósito de marketing, para analizar y crear perfiles de datos con fines de marketing.

Base legal: Las Reglas de Privacidad Transfronterizas (CBPR) establecidas para proporcionar una base para las leyes de privacidad dentro de cada uno de los países miembros de APEC. Hasta ahora, Estados Unidos, Canadá, Japón y México lo han adoptado.

Las pautas de privacidad de datos de CBPR, se aplican a cualquier organización pública o privada que maneje datos personales.

Periodo de tiempo: Hasta que se cierre el acuerdo entre el usuario y glopcare, estos datos pueden procesarse, incluso después de la finalización del acuerdo y si está permitido por la ley.

4. DATOS RECOPILADOS

4.1 Glopcare puede recopilar información personal enviada por el usuario, únicamente para los siguientes propósitos

Ayudarnos a diseñar, desarrollar, entregar y mejorar nuestros servicios, para poder brindar calidad a nuestros usuarios.

Realizar encuestas en línea y otras actividades como publicidad y mercadeo.

4.2. Podemos divulgar tu información personal únicamente en las siguientes condiciones
 
Si tú nos autorizas.

Si está contemplado en la ley aplicable dentro y/o fuera de tu país de residencia, para efectos de procesos legales, solicitudes de litigio, etc.

Por solicitud de las autoridades públicas y/o gubernamentales.

Para proteger los derechos e intereses legales de nuestra plataforma glopcare. Para lo cual se establece lo siguiente

	El suministro de datos personales es voluntario, sin embargo, sin él, no se pueden ofrecer los servicios que requieran de esta información.

	Glopcare, en su función como proveedor de servicios, tiene derecho a publicar en tu tarjeta de presentación: El nombre del usuario o de la empresa del usuario, la dirección del sitio web o cualquier otro dato, que tu como usuario, ingreses en la plataforma glopcare.

	Los destinatarios de datos personales son: Entidades de terceros que entregan y respaldan los sistemas de comunicación de glopcare, utilizados para las operaciones en curso, en virtud de acuerdos activos de tratamiento de datos personales, así como con el uso por parte de las entidades mencionadas, de los recursos técnicos y organizativos adecuados para garantizar los datos de seguridad. Tus datos también se pueden proporcionar a terceros autorizados, por ejemplo: Las fuerzas del orden, para ser utilizados de acuerdo con la normativa vigente, en caso de una solicitud sobre la base legal adecuada, para las necesidades de un proceso legal en curso, etc.

	En algunos casos, tus datos pueden ser transferidos a terceros países, si es necesario para la prestación de alguna funcionalidad del servicio glopcare.

4.3. Tipos de datos recopilados

Entre los tipos de datos que recopila glopcare dentro de su plataforma, se encuentran datos de: 

Ubicación

Datos de ubicación precisa del local, en el cual se prestan los servicios ofrecidos por el usuario.

Información personal

Se refiere a la información propia de cada una de las personas registradas en la plataforma, bien sea como usuario o como cliente, entre las cuales se encuentra: Nombre, dirección de correo, identificadores personales (correo electrónico y/o número de teléfono), número de teléfono y otra información personal (fecha de nacimiento, identidad de género, etc.)

Información financiera
Tarjeta de crédito, tarjeta de débito o número de cuenta bancaria (domiciliaciones de pago?), historial de compras y cualquier otro tipo de información financiera.

Mensajes
 
La plataforma podrá almacenar información de correos electrónicos, mensajes SMS o MMS y otros mensajes en aplicaciones, para realizar seguimiento a las actividades que se registran en la tarjeta de presentación inteligente glopcard.

Fotos y/o videos
 
Fotos y/o videos adjuntados por el usuario acerca de los servicios y/o productos que ofrece en su catálogo y que puede presentar eventualmente en espacios publicitarios destinados para tal fin, así como también, de los servicios realizados a sus clientes, los cuales son totalmente confidenciales y no podrán ser publicados, a menos que el cliente previamente, manifieste su autorización por escrito.

Archivos y documentos
 
Archivos y documentos adjuntados por el usuario, para uso de sus clientes y/o visitantes.

Calendario
 
El calendario será utilizado por ejemplo, para registrar la fecha de las facturas de los servicios prestados por glopcare, programar la publicidad de los usuarios según sus instrucciones, así como para visualizar el historial de sus publicaciones. Y también para registrar la programación, asistencia e historial de citas de los clientes, recordatorios de eventos, etc.

Contactos
 
Información sobre los clientes del usuario, como los nombres, el historial de mensajes, asiduidad y frecuencia de contacto, duración de las interacciones, etc.

Actividad en aplicaciones
 
Visitas a páginas y toques en aplicaciones (información sobre cómo interactúa el usuario con glopcare. Por ejemplo, el número de vistas de una página o las coordenadas de los toques en la pantalla, historial de búsquedas en aplicaciones (información sobre qué ha buscado el usuario en la plataforma glopcare), otros contenidos generados por usuarios (elaboración y actualización de catálogos de servicio y/o producto, publicidad dentro de la aplicación para sus clientes, registro de información de citas de sus clientes, etc.).

Información y rendimiento de aplicaciones.
 
Datos del registro de fallos de tu aplicación.


Identificadores de dispositivo o de otro tipo
 
Identificadores relacionados con un dispositivo, un navegador o una aplicación específicos. Por ejemplo, un número IMEI, una dirección MAC, un ID de dispositivo Widevine, un ID de instalación de Firebase o un identificador de publicidad.

Datos de uso

Información recopilada automáticamente a través de esta plataforma (o servicios de terceros empleados en esta plataforma), que puede incluir: Las direcciones IP o los nombres de dominio de las computadoras utilizadas por los usuarios que usan esta plataforma, las direcciones URI (Identificador Uniforme de Recursos), la hora de la solicitud, el método utilizado para enviar la solicitud al servidor, el tamaño del archivo recibido en respuesta, el código numérico que indica el estado de la respuesta del servidor (resultado exitoso, error, etc.), el país de origen, las características del navegador y el sistema operativo utilizado por el usuario, los diversos detalles de tiempo por visita (por ejemplo, el tiempo dedicado a cada página dentro de la plataforma), y los detalles sobre la ruta seguida dentro la plataforma, con especial referencia, a la secuencia de páginas visitadas y otros parámetros sobre el sistema operativo del dispositivo y/o el entorno tecnológico del usuario.

4.4.  Datos que se recogen

Datos que se transmiten desde la aplicación fuera del dispositivo de los usuarios

Datos personales de cada uno de tus clientes: El único dato personal obligatorio es el correo electrónico y/o número de teléfono, que serán utilizados para identificar al cliente. Son datos opcionales de registro: su identidad de género y su fecha de nacimiento.

Datos correspondientes al registro de cada uno de los servicios prestados a tus clientes: La tarjeta de presentación glopcard, esta diseñada para recopilar toda la información de cada una de las visitas de tus clientes, como por ejemplo: Fecha de la visita, tratamiento realizado, observaciones a ser consideradas para las próximas visitas, precio del servicio realizado, etc. 


4.5.  Datos que se comparten

Datos de usuario recogidos de tu aplicación que se transfieren a un tercero. 

Información de los clientes registrada por los usuarios para su procesamiento estadístico a través del módulo de reportes de la plataforma glopcare o de un tercero, si fuera necesario, con la finalidad de recopilar la información de las visitas de sus clientes y hacer seguimiento de la evolución de los diferentes servicios y/o tratamientos que se realizan, con la finalidad de prestar un mejor servicio, Esta información es totalmente confidencial para el usuario y glopcare.

Información acerca del comportamiento de los clientes y potenciales clientes (visitantes) cuando usan la tarjeta de presentación inteligente glopcard, que será transferida a los servidores Google para su respectivo procesamiento.

Datos transferidos *****

Datos del usuario que se comparten por motivos legales, por ejemplo, para cumplir una obligación legal o atender solicitudes gubernamentales.

Datos anónimos. Es decir, si transfieres datos de usuario que se han anonimizado totalmente para que no se puedan asociar a un usuario concreto. 





Desde las bibliotecas y SDKs de glopcare. Cuando se transfieren los datos recogidos desde la plataforma glopcare para realizar el procesamiento estadístico de la información recolectada.



5. FINALIDAD DE LOS DATOS

Glopcare recopila datos con finalidades de:

Funcionalidad de la aplicación

Habilitar funciones de la aplicación o para autenticar usuarios, etc.

Análisis

Ver cuántos usuarios y/o clientes, usan una función concreta, para monitorizar el estado de la aplicación, para diagnosticar y solucionar errores o fallos, o para mejorar el rendimiento en el futuro, entre otros.

Comunicaciones del desarrollador

Enviar noticias o notificaciones sobre la aplicación o sobre el desarrollador. Por ejemplo, enviar notificaciones de actualizaciones o notificaciones push que informen a los usuarios sobre una actualización importante de seguridad. Todas estas notificaciones podrán ser fácilmente visualizadas por los usuarios, a través de un punto rojo en el ícono de “cuenta”, que se encuentra en el menú principal, ubicado en la barra inferior de la pantalla, y en la campana que encontrarás en el menú desplegable del ícono de “cuenta”, en la cual podrás visualizar las notificaciones pendientes por revisar.

Publicidad o marketing

Mostrar o segmentar anuncios o comunicaciones de marketing para promocionar servicios y/o productos, y/o medir el rendimiento de los anuncios.

Prevención de fraudes, seguridad y cumplimiento

Monitorizar intentos fallidos de inicio de sesión e identificar posibles actividades fraudulentas. Se usan para prevenir fraudes, para proteger la seguridad o para cumplir con la legislación.

Personalización

Se usan para personalizar tu aplicación y ofrecer opciones como mostrar contenido recomendado o sugerencias, acerca de los servicios y productos que ofrecen.

Gestión de cuentas

Permitir a los usuarios que creen cuentas o que inicien sesión en tu aplicación, o para verificar sus credenciales, entre otras. Se usan para configurar y gestionar las cuentas de usuario.  

Los detalles completos sobre cada tipo de datos personales recopilados, se proporcionan en las secciones dedicadas de estas Políticas de Privacidad o mediante textos explicativos específicos, que se muestran antes de la recopilación de datos.

Los datos de uso, son recopilados automáticamente al utilizar esta plataforma. Como la única identificación de tu cuenta es: El correo electrónico, el mismo debe ser proporcionado por el usuario. El usuario recibirá de parte de glopcare, un correo electrónico en diversas situaciones, como por ejemplo: Cuando el sistema te da la bienvenida automatizada, cuando realizas cambios en la información de tu cuenta, cambios en la información de las citas (usuario y clientes), confirmación de pagos, etc.

A menos que se especifique lo contrario, todos los datos solicitados por esta plataforma son obligatorios, y el hecho de no proporcionar estos datos, puede hacer que sea imposible que esta plataforma proporcione sus servicios. En los casos en los que glopcare indique específicamente, que algunos datos no son obligatorios, los usuarios son libres de no comunicar estos datos sin consecuencias para la disponibilidad o el funcionamiento del servicio.
Los usuarios que no estén seguros de qué datos personales son obligatorios, pueden leer la información correspondiente a cada sección, ubicada en la i que se encuentra, en la esquina superior derecha de la pantalla, o pueden ponerse en contacto con un asesor glopcare.

6. MÉTODOS Y LUGAR DE TRATAMIENTO DE LOS DATOS

6.1. Métodos de tratamiento

Glopcare toma las medidas de seguridad adecuadas para evitar el acceso no autorizado, la divulgación, la modificación o la destrucción no autorizada de los datos.
El tratamiento de los datos se realiza mediante ordenadores y/o herramientas informáticas, siguiendo procedimientos organizativos y modalidades estrictamente relacionadas con las finalidades indicadas. 
Además, glopcare permitirá en algunos casos, que los datos puedan ser accesibles a cierto tipo de personas a cargo, involucradas con el funcionamiento de esta plataforma (administración, ventas, marketing, legal, administración del sistema) o partes externas como terceros, (proveedores de servicios técnicos, proveedores de alojamiento, empresas de tecnología de información (TI) y agencias de comunicaciones designadas) si es necesario. La lista actualizada de estas partes se puede solicitar a glopcare en cualquier momento.

6.2. Base legal del tratamiento

Glopcare puede procesar datos personales relacionados con los usuarios, si se aplica alguno de los siguientes casos

Los usuarios han dado su consentimiento para uno o más propósitos específicos. Nota: Según algunas legislaciones, se puede permitir que glopcare procese datos personales hasta que el usuario se oponga a dicho tratamiento ("exclusión voluntaria de la plataforma"), sin tener que depender del consentimiento o de cualquier otra de las siguientes bases legales. Sin embargo, esto no se aplica, si el tratamiento de datos personales está sujeto a la ley de protección de datos, de cada país.

El tratamiento es necesario para el cumplimiento de las obligaciones legales a las que está sujeto el usuario.

El tratamiento está relacionado con una tarea que se lleva a cabo, en interés público o en el ejercicio de la autoridad oficial conferida a glopcare.

El tratamiento es necesario para los fines de los intereses legítimos perseguidos por glopcare o por un tercero.

En cualquier caso, glopcare con gusto ayudará a aclarar la base legal específica que se aplica al tratamiento y, en particular, si el suministro de datos personales es un requisito legal, o un requisito necesario para celebrar un acuerdo.

6.3. Lugar de tratamiento de datos

Los datos se procesan en las oficinas operativas de glopcare y en cualquier otro lugar, donde se encuentren las partes involucradas en el tratamiento.
Dependiendo de la ubicación del usuario, las transferencias de datos, pueden implicar la transferencia de los datos del usuario a un país que no sea el suyo. Para obtener más información sobre el lugar de tratamiento de dichos datos transferidos, los usuarios pueden consultar la sección que contiene detalles sobre el tratamiento de datos personales.
Los usuarios también tienen derecho a conocer la base legal de las transferencias de datos a un país fuera del suyo o a cualquier organización internacional, regida por el derecho internacional público o establecido por dos o más países, como la ONU, y sobre las medidas de seguridad adoptadas por glopcare, para salvaguardar sus datos. Si se lleva a cabo dicha transferencia, los usuarios pueden obtener más información al consultar las secciones relevantes de este documento, o consultar con glopcare, utilizando la información proporcionada en la sección de contacto.

6.4. Tiempo de retención de datos

Los datos personales se procesarán y almacenarán durante el tiempo que requiera el propósito para el que se recopilaron. Por lo tanto

Los datos personales recopilados son para fines relacionados con la ejecución de un servicio entre glopcare y el usuario, se conservarán hasta que dicho servicio se haya cumplido en su totalidad. Estos servicios se prestan bajo la figura legal de planes de suscripción, que se renovarán automáticamente en los términos de servicio, que existan en la fecha de renovación, a menos que glopcare haya cancelado dicho plan. Ten en cuenta que, si cancelas tu plan de suscripción y deseas recuperar esta información, ciertos cargos y restricciones aplican.

Los datos personales recopilados para los fines de los intereses legítimos de glopcare, se conservarán durante el tiempo que sea necesario para cumplir con dichos fines. Los usuarios pueden conocer información específica sobre los intereses legítimos perseguidos por glopcare, poniéndose en contacto con alguno de los asesores de glopcare.

Se puede permitir que glopcare retenga los datos personales durante un período más largo siempre que el usuario haya dado su consentimiento para dicho tratamiento, siempre que dicho consentimiento no sea retirado. Además, glopcare puede estar obligado a conservar los datos personales durante un período más largo, siempre que sea necesario para el cumplimiento de una obligación legal o por orden de una autoridad, cuyo tiempo será estipulado por la ley, de acuerdo al caso.

Una vez que expire el período de retención, los datos personales se eliminarán. Por lo tanto, el derecho de acceso, el derecho de supresión, el derecho de rectificación y el derecho a la portabilidad de los datos, no pueden hacerse valer después de la expiración del período de retención.

6.5. Los fines del tratamiento 

Los datos relativos al usuario se recopilan para permitir a glopcare prestar sus servicios, cumplir con sus obligaciones legales, responder a las solicitudes de ejecución, proteger sus derechos e intereses (o los de sus usuarios o de terceros), detectar cualquier actividad maliciosa o fraudulenta, así como lo siguiente: Análisis, registro y autenticación proporcionados directamente por esta plataforma, manejo de pagos, publicidad, hospedaje o infraestructura backend.
Para obtener información específica sobre los datos personales utilizados para cada propósito, el usuario puede consultar la sección “Información detallada sobre el tratamiento de datos personales”.

6.6. Información detallada sobre el tratamiento de datos personales

Los datos personales se recopilan para los siguientes propósitos, utilizando los siguientes servicios

Venta de servicios y productos online glopcare.

Los datos personales recopilados se utilizan para proporcionar al usuario servicios de diseñar, clasificar, mostrar y vender los servicios y/o productos que ofrece. Estos datos personales, recopilados por esta aplicación, podrían ser usados por ti (usuario), para realizar los pagos a glopcare, por los servicios prestados por esta plataforma.

Publicidad

Este tipo de servicio, permite que los datos del usuario se utilicen con fines de comunicación publicitaria o de promoción. Estas comunicaciones o promociones se muestran en forma de anuncios de imágenes y/o videos en esta plataforma, posiblemente en función de los intereses del usuario.
Esto no significa que todos los datos personales se utilicen con este fin. La información y las condiciones de uso, se muestran a continuación.
Algunos de los servicios enumerados a continuación pueden usar rastreadores para identificar a los usuarios o pueden usar la técnica de reorientación del comportamiento, es decir, mostrar anuncios adaptados a los intereses y el comportamiento del usuario, incluidos los detectados fuera de esta aplicación. Para obtener más información, consulte las políticas de privacidad de los servicios correspondientes.
Los usuarios también pueden optar por no recibir ciertas funciones publicitarias a través de la configuración del dispositivo correspondiente, como la configuración de publicidad del dispositivo para teléfonos móviles o la configuración de anuncios en general.

Publicidad de Google

Google AdSense es un servicio de publicidad proporcionado por Google LLC del país en referencia, según la ubicación desde la que se accede a esta aplicación. Este servicio utiliza la cookie "doubleclick", que rastrea el uso de esta aplicación y el comportamiento del usuario y el cliente, con respecto a los anuncios, productos y servicios ofrecidos.
Los usuarios pueden decidir deshabilitar todas las cookies de doubleclick ingresando a: Configuración de anuncios de Google. Datos personales procesados: Cookies; Datos de uso. 
Lugar de tratamiento: Estados Unidos - Política de privacidad - Exclusión voluntaria.

Analítica

Los servicios contenidos en esta sección permiten a glopcare monitorear y analizar el tráfico web y se pueden utilizar para realizar un seguimiento del comportamiento del usuario. 

Google Analytics

Google Analytics es un servicio de análisis web proporcionado por Google LLC, del país de referencia, según la ubicación desde la que se accede a esta aplicación, ("Google"). Google utiliza los datos recopilados para rastrear y examinar el uso de esta plataforma, para preparar informes sobre sus actividades y compartirlos con otros servicios de Google.
Google puede utilizar los datos recopilados para contextualizar y personalizar los anuncios de su propia red publicitaria. Datos personales procesados: cookies; datos de uso. 
Lugar de tratamiento: Estados Unidos - Política de privacidad - Exclusión voluntaria.

Fuentes de Google

Google Fonts es un servicio de visualización de tipografías prestado por Google LLC, dependiendo de la ubicación desde la que se acceda a esta plataforma, lo que permite a glopcare incorporar contenido de este tipo en sus páginas.
Lugar de tratamiento: Estados Unidos - Política de privacidad.

7.  Visualización de contenido desde plataformas externas 

Este tipo de servicio te permite a ti como usuario, ver contenido alojado en plataformas externas, directamente desde glopcare e interactuar con ellas.
Este tipo de servicio, aún puede recopilar datos de tráfico web para las páginas donde está instalado, incluso cuando los usuarios no las utilizan (esto sólo sucederá si se publicitan contenidos de los usuarios en Google Ads). 

8.  Privacidad del pago de tus planes de suscripción

Glopcare adopta estrictas medidas de seguridad para salvaguardar tu información de pago, por esta razón son escogidas cuidadosamente cada una de las formas de pago, que encontrarás en nuestra plataforma. Ten en cuenta, que tu información de pago se la suministras directamente a los proveedores de servicios de pago, que tenemos asociados a nuestra plataforma, por lo que es recomendable que selecciones el de tu preferencia. Para más detalles, revisa las políticas de privacidad de la pasarela de pago que decidas usar. En general, y a menos que se indique lo contrario, esta plataforma recibirá una notificación del proveedor de servicios de pago correspondiente, cuando el pago se ha completado con éxito.

En este momento las pasarelas de pago disponibles son 

PayPal (PayPal Inc.)

PayPal es un servicio de pago proporcionado por PayPal Inc., que permite a los usuarios realizar pagos en línea. Datos personales procesados: Varios tipos de datos según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Consulte la política de privacidad de PayPal - Política de privacidad.

Stripe (Stripe Inc.)

Stripe es un servicio de pago proporcionado por Stripe Inc. para realizar pagos con tarjeta de débito o crédito . Datos personales procesados: Varios tipos de datos, según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Consulte la política de privacidad de Stripe - Política de privacidad.

Cuando escojas registrar tus pagos a través de WhatsApp Business, Tom nuestro asistente glopcare virtual, te asistirá durante todo el proceso para realizar tu pago. En primer lugar, te indicará las formas de pago disponibles, cuyos detalles debes verificar en la sección de información de pago, ubicada en la pestaña de contratar servicios, cuando haces click sobre el ícono de “cuenta”, que está en la barra del menú principal en el borde inferior de la pantalla. Si una vez que has verificado la información de pago en la plataforma, esta llegara eventualmente a cambiar, debes revisarla nuevamente en la plataforma, para verificar los datos. 
Si decides domiciliar tus pagos, automáticamente se cargará el pago de tu suscripción al instrumento financiero que hayas indicado, sin que tengas que realizar ninguna acción adicional, a menos que alguna de las tarifas de suscripción sean modificadas. 

9.  Infraestructura de alojamiento y backend 

Este tipo de servicio, tiene el propósito de alojar datos y archivos que permiten que esta plataforma se ejecute y se distribuya, así como proporcionar una infraestructura preparada para ejecutar funciones o partes específicas de esta plataforma.
Glopcare usa los SERVICIOS WEB DE AMAZON (AWS) (SERVICIOS WEB DE AMAZON, INC.): Amazon Web Services (AWS) es un servicio de alojamiento y backend, proporcionado por Amazon Web Services, Inc. Datos personales procesados: Varios tipos de datos según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Estados Unidos - Política de privacidad.

10. Registro y autenticación

Al registrarse o autenticarse, los usuarios permiten que esta plataforma los identifique y les dé acceso a los servicios glopcare, dedicados a profesionales del cuidado de la imagen personal.
Dependiendo de lo que se describe a continuación, los terceros pueden proporcionar servicios de registro y autenticación. En este caso, esta plataforma podrá acceder a algunos datos, almacenados por estos servicios de terceros, con fines de registro o identificación. Algunos de los servicios que se enumeran a continuación, también pueden recopilar datos personales con fines de orientación y elaboración de perfiles. Para obtener más información, consulte la descripción de cada servicio.

Autenticación de Facebook

La autenticación de Facebook es un servicio de registro y autenticación proporcionado por Facebook, Inc., según la ubicación desde la que se accede a esta aplicación y está conectada a la red social de Facebook.  Datos personales procesados: Varios tipos de datos según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Estados Unidos - Política de privacidad.

Google OAuth

Google OAuth es un servicio de registro y autenticación proporcionado por Google LLC, según la ubicación desde la que se accede a esta aplicación y está conectada a la red de Google. Datos personales procesados: varios tipos de datos según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Estados Unidos - Política de privacidad.

Autenticación de Instagram

La autenticación de Instagram es un servicio de registro y autenticación proporcionado por Facebook, Inc., según la ubicación desde la que se accede a esta aplicación y está conectada a la red social de Instagram. Datos personales procesados: Varios tipos de datos según se especifica en la política de privacidad del servicio.
Lugar de tratamiento: Estados Unidos - Política de privacidad.

Autenticación con APPLE ID

Distribuido por APPLE Inc., la autenticación de dos factores, es una medida de protección adicional para tu Apple ID, que se diseñó para garantizar que únicamente tú puedas acceder a tu cuenta, aunque alguien más conozca la contraseña. 
Lugar de tratamiento: Estados Unidos - Política de privacidad.

Registro y autenticación proporcionados directamente por esta plataforma

Al registrarse o autenticarse, los usuarios permiten que la plataforma glopcare los identifique y les dé acceso a sus servicios. Los datos personales se recopilan y almacenan sólo con fines de registro (identificación); y para analizar y crear perfiles de datos con fines de marketing. Los datos recabados son únicamente los necesarios para la prestación del servicio solicitado por los usuarios.

Registro directo

El usuario se registra llenando el formulario de registro y proporcionando los datos personales directamente en plataforma glopcare. Datos personales procesados: Dirección de correo electrónico, contraseña.

11. Gestión de etiquetas

Este tipo de servicio ayuda a glopcare a administrar las etiquetas o scripts necesarios en esta plataforma de manera centralizada. Esto da como resultado, que los datos de los usuarios fluyan a través de estos servicios, lo que podría resultar en la retención de estos datos. 

12. Los derechos de los usuarios

12.1. Los usuarios pueden ejercer ciertos derechos con respecto a sus datos procesados por glopcare

En particular, los usuarios tienen derecho a hacer lo siguiente

Retirar su consentimiento en cualquier momento

Los usuarios tienen derecho a retirar su consentimiento, cuando previamente lo hayan dado para el tratamiento de sus datos personales.

Objetar el tratamiento de sus datos

Los usuarios tienen derecho a oponerse al tratamiento de sus datos, si el tratamiento se lleva a cabo, sobre una base legal distinta al consentimiento. 

Acceder a sus datos
Los usuarios tienen derecho a saber si glopcare está procesando los datos; y a obtener información, sobre ciertos aspectos del tratamiento, además de obtener una copia de los datos que se procesan. 

Verificar y buscar rectificación

Los usuarios tienen derecho a verificar la exactitud de sus datos y solicitar su actualización o corrección.

Restringir el tratamiento de sus datos

Los usuarios tienen derecho, en determinadas circunstancias, a restringir el tratamiento de sus datos. En este caso, glopcare no procesará sus datos para ningún otro fin, que no sea su almacenamiento. 

Hacer que se eliminen sus datos personales

Los usuarios tienen derecho, en determinadas circunstancias, a exigir que sus datos sean borrados en glopcare.

Presentar una queja

Los usuarios tienen derecho a presentar un reclamo ante la autoridad de protección de datos competente, siempre y cuando, esta queja este bajo fundamentos legales incumplidos.

12.2. Detalles sobre el derecho a objetar el tratamiento de tus datos

Cuando los datos personales se procesan para un interés público, en el ejercicio de una autoridad oficial conferida a glopcare o para los fines de los intereses legítimos perseguidos por glopcare, los usuarios pueden oponerse a dicho tratamiento, proporcionando un motivo relacionado con su situación particular, para justificar la objeción.
Los usuarios deben saber que, si sus datos personales se procesan con fines de marketing directo, pueden oponerse a ese tratamiento, en cualquier momento, sin proporcionar ninguna justificación. 
Para saber si glopcare está procesando datos personales con fines de marketing directo, los usuarios pueden consultar las secciones relevantes de este documento o consultar algún asesor glopcare.

12.3. ¿Cómo ejercer estos derechos?

Cualquier solicitud para ejercer los derechos de usuario, se puede dirigir a glopcare a través de los datos de contacto proporcionados al inicio de este documento. Estas solicitudes se pueden realizar de forma gratuita y serán atendidas por un asesor glopcare lo antes posible.

12.4. Información adicional sobre la recopilación y tratamiento de datos

Acción legal

Los datos personales del usuario, pueden ser utilizados con fines legales por glopcare en el Tribunal o en las etapas, que conducen a posibles acciones legales, derivadas del uso indebido de esta plataforma o sus servicios relacionados. El usuario declara ser consciente de que se le puede solicitar a glopcare, que revele datos personales a solicitud de las autoridades públicas.

Información adicional sobre los datos personales del usuario

Además de la información contenida en estas Políticas de Privacidad, esta plataforma puede proporcionar al usuario, información adicional y contextual sobre servicios particulares, o la recopilación de datos personales cuando lo solicite.

Registros y mantenimiento del sistema

Para fines de operación y mantenimiento, esta plataforma y cualquier servicio de terceros, pueden recopilar archivos que registren la interacción con esta plataforma (registros del sistema) y utilizar otros datos personales (como la dirección IP) para este propósito.

Información no contenida en estas Políticas de Privacidad

Se puede solicitar a glopcare más detalles sobre la recopilación o el tratamiento de datos personales, en cualquier momento. Consulte la información de contacto al principio de este documento.

¿Cómo se tratan las solicitudes de "no seguimiento"?

Esta aplicación no admite solicitudes de "no rastrear". Para determinar, si alguno de los servicios de terceros que utiliza, cumple con las solicitudes de "no rastrear", lea sus políticas de privacidad.

Cambios a estas Política de Privacidad

Glopcare se reserva el derecho de realizar cambios en estas Políticas de Privacidad, en cualquier momento, notificando a sus usuarios en esta plataforma, en la medida en que sea técnica y legalmente posible, enviando un aviso a los usuarios, a través de cualquier canal de comunicación disponible. Se recomienda, revisar la sección de notificaciones de esta página, ubicada en el menú desplegable al ingresar al ícono “cuenta” y consultar sobre la fecha de la última modificación que se indica al principio de este documento. Si los cambios afectan las actividades de tratamiento realizadas sobre la base del consentimiento del usuario, glopcare deberá recabar un nuevo consentimiento del usuario, si fuera necesario.

13. Políticas de privacidad de cookies

La plataforma glopcare actualmente, no utiliza cookies, sin embargo, algunos proveedores de servicio asociados a nuestra plataforma podrían usarlas, motivo por el cual, si tienes alguna duda, debes revisar las políticas de privacidad de los proveedores de servicio que las usan, dentro de nuestra plataforma.



Cifrado en tránsito: ¿tu aplicación recoge o comparte los datos usando el cifrado en tránsito para proteger el flujo de datos de usuario desde el dispositivo del usuario final hasta el servidor? 
Revisión de seguridad independiente (función opcional disponible próximamente)

Si quieres asegurarte de que tu aplicación esté en condiciones óptimas antes de una revisión de seguridad independiente, te recomendamos que revises la documentación de Google sobre cómo crear aplicaciones seguras y que utilices el MASVS de OWASP para obtener instrucciones adicionales.

