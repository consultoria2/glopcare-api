<tr>
  <td class="header">
    <a href="https://glopcare.com" style="display: inline-block;">
      @if (trim($slot) === 'Laravel')
      <img src="https://laravel.com/img/notification-logo.png" class="logo" alt="Laravel Logo">
      @else
      <img src="https://glopcare.com/img/logo_mail.png" class="logo" alt="glopcare">
      @endif
    </a>
  </td>
</tr>