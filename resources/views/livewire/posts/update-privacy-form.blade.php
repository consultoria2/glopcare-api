<div class="mt-10 sm:mt-0">
    <div class="md:grid md:grid-cols-3 md:gap-6">
        <div class="md:col-span-1 flex justify-between">
            <div class="px-4 sm:px-0">
                <div id="alertPrivacy" style="margin: 10px 0; background-color: #e6fffa; display: none;"
                    class="px-4 py-3 rounded relative" role="alert">
                    <strong class="font-bold">Guardado con éxito</strong>
                </div>
                <h3 class="text-lg font-medium text-gray-900">Editar Privacy</h3>

                <p class="mt-1 text-sm text-gray-600">
                    Aquí puede incluir información sobre qué datos se recopilan, cómo se usan y cómo se protegen.
                </p>
            </div>

            <div class="px-4 sm:px-0">

            </div>
        </div>

        <div class="mt-5 md:mt-0 md:col-span-2">
            <div class="px-4 py-5 bg-white sm:p-6 shadow sm:rounded-tl-md sm:rounded-tr-md">
                <div class="grid grid-cols-6 gap-6">
                    <div class="col-span-6">
                        <textarea name="privacies_content" id="privacies_content"
                            class="form-control @error('privacies_content') is-invalid @enderror">
                            @if ($current_privacy)
                            {{ $current_privacy->privacies_content }}
                            @else
                            Agregue contenido
                            @endif
                        </textarea>
                    </div>
                </div>
            </div>

            <div
                class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6 shadow sm:rounded-bl-md sm:rounded-br-md">


                <button id="showButtonPrivacy" type="button" onclick="submitPrivacy()"
                    class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">
                    Guardar Privacy
                </button>

            </div>
        </div>
    </div>
</div>
<script>
    tinymce.init({
        selector: '#privacies_content',
        apiKey: 'no-api-key',
        height: 500,
        menubar: false,
        plugins: 'advlist autolink lists link image charmap preview anchor searchreplace visualblocks code fullscreen insertdatetime media table code help wordcount',
        toolbar: 'undo redo | blocks | bold italic backcolor | alignleft aligncenter alignright alignjustify bullist numlist outdent indent | removeformat | help',
        content_style: 'body { font-family: Helvetica, Arial, sans-serif; font-size: 14px; }'
    });

    function submitPrivacy() {
        showButtonPrivacy.disabled = true;
        var editor = tinymce.get('privacies_content');
        var privacies_content = editor.getContent();
        console.log(privacies_content);
        axios.post('{{ route('api.privacy-update') }}', {
                privacies_content: privacies_content,
            })
            .then(response => {
                console.log(response);
                var alertPrivacy = document.getElementById("alertPrivacy");
                alertPrivacy.style.display = "block";
                setTimeout(function() {
                    alertPrivacy.style.display = "none";
                    showButtonPrivacy.disabled = false;
                }, 5000);
            })
            .catch(error => {
                showButtonPrivacy.disabled = false;
                console.log(error);
            });
    }
</script>
