
<form wire:submit.prevent="submit">
    <div class="form-group">
        <label for="exampleInputName">Name</label>
        <input type="text" class="form-control" id="exampleInputName" placeholder="Enter name" wire:model="name">
        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <br/>
  
    <div class="form-group">
        <label for="exampleInputEmail">Email</label>
        <input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter email" wire:model="email">
        @error('email') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <br/>
  
    <div class="form-group">
        <label for="exampleInputEmail">Username</label>
        <input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter username" wire:model="username">
        @error('username') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <br/>

    <div class="form-group">
        <label for="exampleInputEmail">Password</label>
        <input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter password" wire:model="password">
        @error('password') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <br/>
  
    <button type="submit" class="btn btn-primary">Save User</button>
</form>