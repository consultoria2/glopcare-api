<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Order Detail') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Order Status') }} {{ $order->status }}
            </h2>
            <a href="{{ route('order.status', ['status' => 'inprogress', 'orderId' => $order->id]) }}">
                <x-jet-button href="{{ route('order.status', ['status' => 'inprogress', 'orderId' => $order->id]) }}">
                    {{ __('Set In Progress') }}
                </x-jet-button>
            </a>
            <a href="{{ route('order.status', ['status' => 'delivered', 'orderId' => $order->id]) }}">
                <x-jet-button href="{{ route('order.status', ['status' => 'delivered', 'orderId' => $order->id]) }}">
                    {{ __('Set Delivered') }}
                </x-jet-button>
            </a>
            <a href="{{ route('order.status', ['status' => 'cancelled', 'orderId' => $order->id]) }}">
                <x-jet-button href="{{ route('order.status', ['status' => 'cancelled', 'orderId' => $order->id]) }}">
                    {{ __('Cancel Order') }}
                </x-jet-button>
            </a>
            <br>
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="bg-gray-200 bg-opacity-25">

                    <livewire:order-table :orderId="$order->id" />
                </div>
            </div>
            <br>

            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Order Payment Methods') }}
            </h2>
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="bg-gray-200 bg-opacity-25">

                    <livewire:order-payment-methods-table :orderId="$order->id" />
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
