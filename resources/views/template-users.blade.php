<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Visibilidad de la Plantilla') }}
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ is_null($templateUsers->first()) ? 'Esta plantilla la pueden ver todos' : 'Esta plantilla solo la pueden ver los usuarios seleccionados' }}
      </h2>
      <br>
      <h3 class="font-semibold text-l text-gray-800 leading-tight">
        {{ is_null($templateUsers->first()) ? 'Al agregar nuevos usuarios, controla la visibilidad. Las plantillas que ya se hayan creado usando esta plantilla seguirán funcionando' : 'Si desactiva todos los usuarios, la plantilla será pública' }}
      </h3>
      <br>
      <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <div class="bg-gray-200 bg-opacity-25">
          <livewire:template-users-table :templateId="$templateId" :selected="$selected"/>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>