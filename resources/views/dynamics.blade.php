<html>
    <head>
    @if($previewType == 'glopcard')
        <title>Glopcare | {{isset($snapshot->alias_name) ? $snapshot->alias_name : 'Usuario Glopcare'}}</title>
        <meta property="og:title" content="Glopcare | {{isset($snapshot->alias_name) ? $snapshot->alias_name : 'Usuario Glopcare'}}">
        <meta property="og:description" content="{{isset($snapshot->professional_title) ? $snapshot->professional_title : ''}} | {{isset($snapshot->professional_description) ? $snapshot->professional_description : ''}}">
        <meta property="og:url" content="https://glopcare.com/{{$user->username}}">
        <meta property="og:type" content="website" />
        <meta property="og:image" content="https://glopcare.com/storage/uploads/{{$avatar}}?t=12345">
        <!-- <meta property="og:image:width" content="200">
        <meta property="og:image:height" content="200"> -->
        <meta property="og:image:type" content="image/png" />
    @endif
    </head>
    <script>
        const isCrawler = @json($isCrawler);
        if (!isCrawler) {
            setTimeout(function(){
                window.location.href = 'https://glopcard.glopcare.com/{{$previewData->uid}}';
            }, 100);
        }
      </script>
</html>