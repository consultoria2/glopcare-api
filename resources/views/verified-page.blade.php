<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />


        <div class="mb-4 font-medium text-sm text-green-600">
            @if ($code == '002') <p class="text-center">
                <strong>Usuario ya ha sido autenticado</strong>
            </p>
       
    @elseif ($code == '001')
        <p class="text-center">
            <strong>Tu cuenta ha sido confirmada</strong><br>
            Ya puedes ingresar a tu App Glopcare
        </p>
    @endif
        </div>


    </x-jet-authentication-card>
</x-guest-layout>
