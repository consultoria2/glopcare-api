@component('mail::message')
    {{-- Greeting --}}
    @if (!empty($greeting))
        # {{ $greeting }}
    @else
        # @lang('Hello!')
    @endif
    
    @slot('subcopy')
        <span class="break-all">Correo: {{ $message->email }}</span>
        <p class="break-all">{{ $message->message }}</p>
    @endslot
@endcomponent
