@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{!! $line !!}

@endforeach

@if ($order->lines->where('product.product_type', 'product')->count() > 0)
@component('mail::table')
| Productos | Precio | Cantidad | Subtotal |
|:--------- | ------:| --------:| --------:|
@foreach ($order->lines->where('product.product_type', 'product') as $item)
| {{ $item->product->product_name }} | {{ $item->net_price >= 0.01 ? $item->net_price : 'N/A' }} | {{ $item->qty }} | {{ ($item->qty  * $item->net_price) >= 0.01 ? ($item->qty  * $item->net_price) : 'N/A' }} |
@endforeach
@endcomponent
@endif

@if ($order->lines->where('product.product_type', 'service')->count() > 0)
@component('mail::table')
| Servicios | Precio | Cantidad | Subtotal |
|:--------- | ------:| --------:| --------:|
@foreach ($order->lines->where('product.product_type', 'service') as $item)
| {{ $item->product->product_name }} | {{ $item->net_price >= 0.01 ? $item->net_price : 'N/A' }} | {{ $item->qty }} | {{ ($item->qty  * $item->net_price) >= 0.01 ? ($item->qty  * $item->net_price) : 'N/A' }} |
@endforeach
@endcomponent
@endif

@if (!empty($payment_methods))
@component('mail::table')
| Método de Pago | Monto | Número de Confirmación |
|:------------------:|:-----:|:---------------------:|
@foreach ($payment_methods as $payment_method)
| {{ $payment_method['payment_method_name'] }} | {{ $payment_method['amount'] }} | {{ $payment_method['confirmation_number'] }} |
@endforeach
@endcomponent
@endif


{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{!! $line !!}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Saludos'),<br>
{{ 'Somos ' . config('app.name') }}
<br>
{{ 'La Nueva Forma  de Acercarte a tus Clientes' }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "Si estas teniendo problemas con el botón \":actionText\", copia y pega esta URL \n".
    'en tu navegador web:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
