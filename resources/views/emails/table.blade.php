@component('mail::table')
    | Nombre | Precio | Cantidad | Subtotal |
    |:------:| ------:| --------:| --------:|
    @foreach ($order->lines as $item)
        | {{ $item->product_name }} | {{ $item->net_price }} | {{ $item->qty }} | {{ $item->qty  * $item->net_price }} |
    @endforeach
@endcomponent