<x-guest-layout>
    <div class="pt-4">
        <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0">
            <div><br>
                <x-jet-authentication-card-logo />
            </div>

            <div class="w-full sm:max-w-2xl mt-6 p-6 bg-white shadow-md overflow-hidden sm:rounded-lg prose">
                
<p class=MsoNormal><b><span lang=ES-MX style='font-family:"Arial",sans-serif;
mso-ansi-language:ES-MX'>Para eliminar tu cuenta sigue los siguientes pasos<o:p></o:p></span></b></p>

<p class=MsoListParagraphCxSpFirst ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Inicia
sesión en <span class=SpellE>glopcare</span><o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Una
vez dentro, presiona el menú “Cuenta”<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Selecciona
la pestaña “Configuración”<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Presiona
la opción “Eliminar cuenta”<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpMiddle ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Confirma
que deseas eliminar tu cuenta seleccionando la opción “ELIMINAR”. Esta acción
no se puede deshacer. Una vez que elimines tu cuenta, se borraran todos tus
registros asociados y relacionados en “<span class=SpellE>glopcare</span>”<o:p></o:p></span></p>

<p class=MsoListParagraphCxSpLast ><![if !supportLists]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-fareast-font-family:Arial;
mso-ansi-language:ES-MX'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=ES-MX style='font-family:"Arial",sans-serif;mso-ansi-language:ES-MX'>Recibe
un correo electrónico y/o SMS confirmando la eliminación de tu cuenta<o:p></o:p></span></p>

<p class=MsoNormal><b><span lang=ES-MX style='mso-ansi-language:ES-MX'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span lang=ES-MX style='mso-ansi-language:ES-MX'><o:p>&nbsp;</o:p></span></b></p>


            </div>
        </div>
    </div>
</x-guest-layout>